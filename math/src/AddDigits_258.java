/**
 * Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.
 * <p>
 * Example:
 * <p>
 * Input: 38
 * Output: 2
 * Explanation: The process is like: 3 + 8 = 11, 1 + 1 = 2.
 * Since 2 has only one digit, return it.
 * Follow up:
 * Could you do it without any loop/recursion in O(1) runtime?
 */
public class AddDigits_258 {
    public static void main(String[] args) {
        System.out.println("addDigits(81) = " + addDigits(81));
        System.out.println("addDigits(137) = " + addDigits(137));
        System.out.println("addDigits(38 = " + addDigits(38));
    }

    public static int addDigits(int num) {
        return (num == 0) ? 0 : (num % 9 == 0) ? 9 : num % 9;
    }
}
