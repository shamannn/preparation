import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.pow;

/**
 * Write all solutions for a^3+b^3 = c^3 + d^3, where a, b, c, d lie between [0, 10^5].
 *
 * Required space is actually O(n^2 logn), where n is the range (10^5),
 * because to represent 10^5 integers you need ceil(log_2(10^15))  = 50 bits.
 * So, you actually need something like 500,000,000,000 bits (+ overhead for map and list) which is ~58.2 GB (+ overhead).
 * Since for most machines it is a bit too much - you might want to consider storing the data on disk,
 * or if you have 64bits machine - just store in into "memory" and let the OS and virtual memory system do this as best as it can.
 */
public class CubicEquiation_CrackingTheCodingInterview {
    public static void main(String[] args) {
        printCubicPairs();
    }

    private static void printCubicPairs() {
        Map<Long, List<Pair>> hashMap = new HashMap<>();
//        int n = (int) pow(10, 5); // 100000, we need 59 Gb ram with this setting
        int n = (int) pow(10, 3); // we need 59 Gb ram with this setting

        for(int i = 1; i <= n; i++) { // 1 0r 0? question states 0
            for(int j = 1; j <= n; j++) {
                long sum = (long) (pow(i, 3) + pow(j, 3));

                if(hashMap.containsKey(sum)) {
                    List<Pair> list = hashMap.get(sum);
                    for(Pair p : list) {
                        System.out.println(i + " " + j + " " + p.a + " " + p.b);
                    }
                } else {
                    List<Pair> list = new ArrayList<>();
                    hashMap.put(sum, list);
                }

                hashMap.get(sum).add(new Pair(i, j));
            }
        }
    }
}

class Pair {
    int a;
    int b;

    Pair(int x, int y) {
        a = x;
        b = y;
    }
}