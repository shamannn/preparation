public class MultiplyStrings_43 {
    public String multiply(String num1, String num2) {
        if (num1 == null || num2 == null) {
            return null;
        }

        int[] resultArray = new int[num1.length() + num2.length()];

        for (int i = num1.length() - 1; i >= 0; i--) {
            for (int j = num2.length() - 1; j >= 0; j--) {
                int first = num1.charAt(i) - '0';
                int second = num2.charAt(j) - '0';
                resultArray[i + j + 1] += first * second;
            }
        }

        int carryOver = 0;
        for (int i = resultArray.length - 1; i >= 0; i--) {
            int value = (resultArray[i] + carryOver) % 10;
            carryOver = (resultArray[i] + carryOver) / 10;
            resultArray[i] = value;
        }

        StringBuilder result = new StringBuilder();
        boolean leadingZero = (resultArray[0] == 0);

        for (int i = 0; i < resultArray.length; i++) {
            if (resultArray[i] != 0) {
                leadingZero = false;
            }
            if (!leadingZero) {
                result.append(resultArray[i]);
            }
        }
        if (result.length() == 0) {
            result.append("0");
        }

        return result.toString();
    }
}
