import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A group of two or more people wants to meet and minimize the total travel distance. You are given a 2D grid of values 0 or 1, where each 1 marks the home of someone in the group. The distance is calculated using Manhattan Distance, where distance(p1, p2) = |p2.x - p1.x| + |p2.y - p1.y|.
 * <p>
 * Example:
 * <p>
 * Input:
 * <p>
 * 1 - 0 - 0 - 0 - 1
 * |   |   |   |   |
 * 0 - 0 - 0 - 0 - 0
 * |   |   |   |   |
 * 0 - 0 - 1 - 0 - 0
 * <p>
 * Output: 6
 * <p>
 * Explanation: Given three people living at (0,0), (0,4), and (2,2):
 * The point (0,2) is an ideal meeting point, as the total travel distance
 * of 2+2+2=6 is minimal. So return 6.
 */
public class BestMeetingPoint_296 {
    public int minTotalDistance(int[][] grid) {
        if (grid == null) {
            return -1;
        }

        // let's limit the area where we search for meeting point
        List<Integer> friendsI = new ArrayList<>();
        List<Integer> friendsJ = new ArrayList<>();

        // find points and minimum and maximum I and J
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    friendsI.add(i);
                    friendsJ.add(j);
                }
            }
        }

        Collections.sort(friendsJ);

        // median (middle of array) is the best meeting point in case of Manhattan distance
        // because you need to have equal number of "friends" on both sides of meeting point
        // PROVE - if you move towards lower number of friends, the total distance increases
        // same if you move opposite direction.
        int medianI = friendsI.get(friendsI.size() / 2), medianJ = friendsJ.get(friendsJ.size() / 2);


        /*System.out.println(medianI);
        System.out.println(medianJ);
        */

        int minimumDistanceI = 0, minimumDistanceJ = 0;


        for (int i : friendsI) {
            minimumDistanceI += Math.abs(medianI - i);
        }

        for (int j : friendsJ) {
            minimumDistanceJ += Math.abs(medianJ - j);
        }

        return minimumDistanceI + minimumDistanceJ;
    }

}
