import java.util.HashSet;
import java.util.Set;

/**
 * Write an algorithm to determine if a number is "happy".
 * <p>
 * A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.
 * <p>
 * Example:
 * <p>
 * Input: 19
 * Output: true
 * Explanation:
 * 12 + 92 = 82
 * 82 + 22 = 68
 * 62 + 82 = 100
 * 12 + 02 + 02 = 1
 */
public class HappyNumber_202 {
    public boolean isHappy(int n) {
        if (n == Integer.MAX_VALUE) {
            return false;
        }

        Set<Integer> previousNumbers = new HashSet<>();

        // we avoid loop by checking if we already met this number earlier
        while (previousNumbers.add(n)) {
            if (n == 1) {
                return true;
            }
            else {
                n = getSum(n);
            }
        }

        return false;
    }

    private int getSum(int i) {
        int sum = 0;
        while (i > 0) {
            sum += (i % 10) * (i % 10);
            i = i / 10;
        }

        return sum;
    }
}
