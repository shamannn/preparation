/**
 * Count the number of prime numbers less than a non-negative number, n.
 * <p>
 * Example:
 * <p>
 * Input: 10
 * Output: 4
 * Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
 */
public class CountPrimes_204 {

    /**
     * Sieve of Eratosphenus - we take first prime, then multiply it by any number until product is less than our N.
     * Then we take next prime and so on... until N. So further we move more non prime numbers are crossed out
     */
    public int countPrimes(int n) {
        int count = 0;

        boolean[] sieve = new boolean[n];

        for (int i = 2; i < n; i++) {
            if (!sieve[i]) {
                for (int j = 2; i * j < n; j++) {
                    sieve[i * j] = true;

                    // System.out.println(i);

                    //System.out.println("prime");
                }
                count++;
            }
        }
        return count;
    }

}
