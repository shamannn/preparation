import java.util.HashMap;
import java.util.Map;

/**
 * Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
 * <p>
 * Example 1:
 * <p>
 * Input: [[1,1],[2,2],[3,3]]
 * Output: 3
 * Explanation:
 * ^
 * |
 * |        o
 * |     o
 * |  o
 * +------------->
 * 0  1  2  3  4
 * Example 2:
 * <p>
 * Input: [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]]
 * Output: 4
 * Explanation:
 * ^
 * |
 * |  o
 * |     o        o
 * |        o
 * |  o        o
 * +------------------->
 * 0  1  2  3  4  5  6
 */
public class MaxPointsOnALine_149 {
    /**
     * Definition for a point.
     * class Point {
     * int x;
     * int y;
     * Point() { x = 0; y = 0; }
     * Point(int a, int b) { x = a; y = b; }
     * }
     */

    public int maxPoints(Point[] points) {
        if (points == null) {
            return 0;
        }
        int n = points.length, result = 0;
        if (n <= 2) {
            return n;
        }
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            map.clear();
            int overlap = 0, max = 0;
            for (int j = i + 1; j < n; j++) {
                int x = points[j].x - points[i].x;
                int y = points[j].y - points[i].y;
                if (x == 0 && y == 0) {
                    overlap++;
                    continue;
                }
                int gcd = gcd(x, y); // gcd will never be zero.
                x /= gcd;
                y /= gcd;

                String key = x + ":" + y;
                if (map.containsKey(key)) {
                    map.put(key, map.get(key) + 1);
                }
                else {
                    map.put(key, 1);
                }
                max = Math.max(max, map.get(key));
            }
            result = Math.max(result, max + overlap + 1);
        }
        return result;
    }

    private int gcd(int a, int b) {

        if (b == 0) {
            return a;
        }
        else {
            return gcd(b, a % b);
        }

    }


    class Point {
        int x;
        int y;

        Point() {
            x = 0;
            y = 0;
        }

        Point(int a, int b) {
            x = a;
            y = b;
        }
    }
}
