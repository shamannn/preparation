import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This was asked at Morgan Stanley interview.
 * Found solution at https://stackoverflow.com/questions/931323/getting-friends-within-a-specified-degree-of-separation
 */
public class FriendsOfCertainDegree_MorganStanley {

    public static void getFriends(Person person, int degree, Set<Person> friendsSet){
        friendsSet.add(person); // Key Point!! Add person itself, not friends below

        if (degree > 0){
            Set<Person> friends = person.getFriends();

            for (Person friend : friends) {
                getFriends(friend, degree -1, friendsSet);
            }
        }
    }

    public static void main(String[] args) {
        Person person = new Person();
        HashSet<Person> friendsSet = new HashSet<>();


        getFriends(person, 1, friendsSet);
        System.out.println("degree 1 friendsSet.size() = " + friendsSet.size());

        friendsSet = new HashSet<>();
        getFriends(person, 2, friendsSet);
        System.out.println("degree 2 friendsSet.size() = " + friendsSet.size());
    }

    private static class Person{
        public static final int MAX_NUMBER_OF_FRIENDS = 100;
        private String name;

        public Set<Person> getFriends(){
            int numberOfFriends = ThreadLocalRandom.current().nextInt(0, MAX_NUMBER_OF_FRIENDS + 1);; // for testing only!!

            System.out.println("numberOfFriends = " + numberOfFriends);

            Set<Person> friendsSet = new HashSet<>();

            for(int i=0; i < numberOfFriends; i++){
                friendsSet.add(new Person());
            }

            return friendsSet;
        }
    }
}
