import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * There are a total of n courses you have to take, labeled from 0 to n-1.
 * <p>
 * Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]
 * <p>
 * Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?
 * <p>
 * Example 1:
 * <p>
 * Input: 2, [[1,0]]
 * Output: true
 * Explanation: There are a total of 2 courses to take.
 * To take course 1 you should have finished course 0. So it is possible.
 * Example 2:
 * <p>
 * Input: 2, [[1,0],[0,1]]
 * Output: false
 * Explanation: There are a total of 2 courses to take.
 * To take course 1 you should have finished course 0, and to take course 0 you should
 * also have finished course 1. So it is impossible.
 * Note:
 * <p>
 * The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a graph is represented.
 * You may assume that there are no duplicate edges in the input prerequisites.
 * Hints:
 * <p>
 * This problem is equivalent to finding if a cycle exists in a directed graph. If a cycle exists, no topological ordering exists and therefore it will be impossible to take all courses.
 * Topological Sort via DFS - A great video tutorial (21 minutes) on Coursera explaining the basic concepts of Topological Sort.
 * Topological sort could also be done via BFS.
 */
public class CourseSchedule_207 {
    /**
     * So in this method we first build a graph, then iterate through with Depth First Search recursively,
     * marking nodes with VISITING when we start and COMPLETED when we finished.
     * If we find while processing that node is already in VISITING state then we have a problem.
     */
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        if (prerequisites == null || prerequisites.length == 0) {
            return true;
        }

        Graph graph = buildGraph(prerequisites);

        for (Integer startNodeId : graph.nodes.keySet()) {
            //           System.out.println("startNodeId = " + startNodeId);

            if (!dfs(graph.getNode(startNodeId))) {
                return false;
            }
        }

        return true;
    }

    private boolean dfs(Node node) {
        //System.out.println("node.id = " +node.id);
        //System.out.println("node.state = " +node.state);

        if (node.state == State.VISITING) {
            return false;
        }
        if (node.state == State.NOT_VISITED) {
            node.state = State.VISITING;

            Set<Node> adjacent = node.getAdjacent();

            for (Node adj : adjacent) {
                //System.out.println("adj.id = " +adj.id);
                //System.out.println("adj.state = " +adj.state);

                if (!dfs(adj)) {
                    return false;
                }
            }

            node.state = State.COMPLETED;

        }

        return true;

    }

    private Graph buildGraph(int[][] array) {
        Graph graph = new Graph();

        for (int i = 0; i < array.length; i++) {
            Integer from = array[i][0];
            Integer to = array[i][1];

            Node fromNode = graph.getNode(from);
            if (fromNode == null) {
                fromNode = new Node(from);
            }

            Node toNode = graph.getNode(to);
            if (toNode == null) {
                toNode = new Node(to);
            }

            fromNode.addAdjacent(toNode);

            graph.addNode(from, fromNode);
            graph.addNode(to, toNode);
        }

        return graph;
    }

    class Graph {
        Map<Integer, Node> nodes = new HashMap<>();

        public void addNode(int id, Node node) {
            nodes.put(id, node);
        }

        public Node getNode(int id) {
            return nodes.get(id);
        }
    }

    class Node {
        int id = -1;
        State state;

        Set<Node> adjacent = new HashSet<>();

        public Node(int id) {
            this.id = id;
            state = State.NOT_VISITED;
        }

        public void addAdjacent(Node node) {
            //System.out.println("addAdjacent id = " + id + " adjacent to node " + node.id);

            adjacent.add(node);
        }

        public Set<Node> getAdjacent() {
            return adjacent;
        }
    }

    enum State {NOT_VISITED, VISITING, COMPLETED}
}