import java.util.*;

/**
 There are a total of n courses you have to take, labeled from 0 to n-1.

 Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

 Given the total number of courses and a list of prerequisite pairs, return the ordering of courses you should take to finish all courses.

 There may be multiple correct orders, you just need to return one of them. If it is impossible to finish all courses, return an empty array.

 Example 1:

 Input: 2, [[1,0]]
 Output: [0,1]
 Explanation: There are a total of 2 courses to take. To take course 1 you should have finished
 course 0. So the correct course order is [0,1] .
 Example 2:

 Input: 4, [[1,0],[2,0],[3,1],[3,2]]
 Output: [0,1,2,3] or [0,2,1,3]
 Explanation: There are a total of 4 courses to take. To take course 3 you should have finished both
 courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
 So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3] .
 Note:

 The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a graph is represented.
 You may assume that there are no duplicate edges in the input prerequisites.
 Hints:

 This problem is equivalent to finding the topological order in a directed graph. If a cycle exists, no topological ordering exists and therefore it will be impossible to take all courses.
 Topological Sort via DFS - A great video tutorial (21 minutes) on Coursera explaining the basic concepts of Topological Sort.
 Topological sort could also be done via BFS.
 */
public class CourseSchedule_II_210 {
    /**
     * So in this method we first build a graph, then iterate through with Depth First Search recursively,
     * marking nodes with VISITING when we start and COMPLETED when we finished.
     * If we find while processing that node is already in VISITING state then we have a problem.
     */
    public int[] findOrder(int numCourses, int[][] prerequisites) {

        if (prerequisites == null || prerequisites.length == 0){

            int[] defaultArray = new int[numCourses];

            for (int i=0; i < numCourses; i++){
                defaultArray[i] = i;
            }

            return defaultArray;
        }


        Graph graph = buildGraph(prerequisites);
        List<Integer> result = new LinkedList<>();

        boolean noCycles = dfs(graph, result);

        //System.out.println("noCycles " + noCycles);

        if(noCycles){
            int[] resultArray = new int[numCourses];

            if (result.size() < numCourses){
                for(int i=0; i< numCourses; i++){
                    if (!result.contains(i)){// could be faster
                        //System.out.println("ADDING  " + i);

                        result.add(0, i);
                    }
                }
            }

            //System.out.println("result.size()  " + result.size());
            //System.out.println("numCourses  " + numCourses);


            for(int j=0; j< result.size(); j++){
                resultArray[j] = result.get(j);
            }

            return resultArray;
        }
        else{
            return new int[]{}; // error case
        }
    }

    private boolean dfs(Graph graph, List<Integer> result){
        for(Integer nodeId: graph.nodes.keySet()){
            //System.out.println("------nodeId " + nodeId);


            Node node = graph.nodes.get(nodeId);

            if(!dfs(node, result)){
                return false;
            }
        }
        return true;
    }

    private boolean dfs(Node node, List<Integer> result){
        //System.out.println("node.id " + node.id);
        //System.out.println("node.state " + node.state);

        if(node.state == State.VISITING){
            return false;
        }
        if(node.state == State.BLANK){
            node.state = State.VISITING;

            Set<Node> adjacent = node.getAdjacent();

            for(Node adj : adjacent){
                //System.out.println("adj.id " + adj.id);

                if(!dfs(adj, result)){
                    return false;
                }
            }

            result.add(node.id);

            node.state = State.COMPLETED;
        }

        return true;
    }


    private Graph buildGraph(int[][] prerequisites){
        Graph graph = new Graph();

        for (int i =0; i < prerequisites.length; i++){
            int fromKey = prerequisites[i][0];

            Node from = graph.nodes.get(fromKey);
            if (from == null){
                from = new Node(fromKey);
                graph.addNode(fromKey, from);
            }


            int toKey = prerequisites[i][1];

            Node to = graph.nodes.get(toKey);
            if (to == null){
                to = new Node(toKey);
                graph.addNode(toKey, to);
            }


            //System.out.println("from.id " + from.id);
            //System.out.println("to.id " + to.id);


            from.addAdjacent(to);
        }

        return graph;
    }

    class Graph{
        Map<Integer, Node> nodes = new HashMap<>();

        void addNode(int id, Node node){
            nodes.put(id, node);
        }
    }

    class Node{
        int id;
        State state;

        Set<Node> adjacent = new HashSet<>();

        Node(int id){
            this.id = id;
            state = State.BLANK;
        }

        void addAdjacent(Node adj){
            //System.out.println(id + " is adjacent to " + adj.id);

            adjacent.add(adj);
        }

        Set<Node> getAdjacent(){
            return adjacent;
        }
    }

    enum State{BLANK, VISITING, COMPLETED}
}