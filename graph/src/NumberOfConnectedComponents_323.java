import java.util.*;

/**
 * Given n nodes labeled from 0 to n - 1 and a list of undirected edges (each edge is a pair of nodes), write a function to find the number of connected components in an undirected graph.
 * <p>
 * Example 1:
 * <p>
 * Input: n = 5 and edges = [[0, 1], [1, 2], [3, 4]]
 * <p>
 * 0          3
 * |          |
 * 1 --- 2    4
 * <p>
 * Output: 2
 * Example 2:
 * <p>
 * Input: n = 5 and edges = [[0, 1], [1, 2], [2, 3], [3, 4]]
 * <p>
 * 0           4
 * |           |
 * 1 --- 2 --- 3
 * <p>
 * Output:  1
 * Note:
 * You can assume that no duplicate edges will appear in edges. Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.
 */
public class NumberOfConnectedComponents_323 {
    public int countComponents(int n, int[][] edges) {
        if (edges == null || n == 0) {
            return 0;
        }
        else if (n == 1) {
            return 1;
        }

        Map<Integer, ArrayList<Integer>> graphMap = new HashMap<>();
        for (int i = 0; i < n; i++) {
            graphMap.put(i, new ArrayList<>()); // vertices and adjacency lists
        }

        for (int i = 0; i < edges.length; i++) {
            graphMap.get(edges[i][0]).add(edges[i][1]); // populating adjacencies
            graphMap.get(edges[i][1]).add(edges[i][0]);
        }


        int count = 0;
        Set<Integer> visited = new HashSet<>();

        for (int i = 0; i < n; i++) {
            if (visited.add(i)) {
                dfs(graphMap, i, visited);
                count++;
            }
        }

        System.out.println(visited.size());

        // if there are vertices which are not mentioned in edges we should consider them as components
        for (int i = 0; i < n; i++) {
            if (!visited.contains(i)) {
                count++;
            }
        }

        return count;
    }

    // Depth First Search
    private void dfs(Map<Integer, ArrayList<Integer>> graphMap, int i, Set<Integer> visited) {

        for (int adjacent : graphMap.get(i)) {
            if (visited.add(adjacent)) {
                dfs(graphMap, adjacent, visited);
            }
        }
    }

}
