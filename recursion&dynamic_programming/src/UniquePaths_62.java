/**
 * A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).
 * <p>
 * The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).
 * <p>
 * How many possible unique paths are there?
 * <p>
 * **Solution**
 * This can be done recursively in a following way, but time complexity of this solution is O(2^(m+n)):
 * <p>
 * class Solution {
 * public int uniquePaths(int m, int n) {
 * if (m==1 || n==1){
 * return 1;
 * }
 * return uniquePaths(m-1, n) + uniquePaths(m, n-1);
 * }
 * }
 * <p>
 * So below we used memoization(dynamic programming) to reduce time complexity to O(m*n).
 */
public class UniquePaths_62 {
    public static void main(String[] args) {
        System.out.println("uniquePaths(3,2) = " + uniquePaths(3, 2));
        System.out.println("uniquePaths(3,2) = " + uniquePaths(10, 10));
    }

    public static int uniquePaths(int m, int n) {
        int[][] memo = new int[m][n];

        for (int i = 0; i < m; i++) {
            memo[i][0] = 1;
        }

        for (int i = 0; i < n; i++) {
            memo[0][i] = 1;
        }


        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                memo[i][j] = memo[i - 1][j] + memo[i][j - 1];
            }
        }

        return memo[m - 1][n - 1];
    }
}
