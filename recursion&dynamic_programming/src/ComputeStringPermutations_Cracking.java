/**
 * Number of permutations of a string of unique characters is n!. This is formual from combinatorics.
 *
 * And below is recursive solution. There is also iterative way of calculating factorial with loop.
 */
public class ComputeStringPermutations_Cracking {
    public static void main(String[] args) {
        System.out.println("permutations(\"abfeg\") = " + permutations("abfeg"));
        System.out.println("permutations(\"ab\") = " + permutations("ab"));
        System.out.println("permutations(\"\") = " + permutations(""));
    }

    public static int permutations(String input) {
        if (input == null) {
            return 0;
        }

        return factorial(input.length());
    }

    public static int factorial(int n) {
        if ((n == 0) ||  (n == 1)) {
            return 1;
        }

        return n * factorial(n - 1);
    }
}
