import java.util.LinkedList;
import java.util.List;

/**
 * Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
 * <p>
 * For example, given n = 3, a solution set is:
 * <p>
 * [
 * "((()))",
 * "(()())",
 * "(())()",
 * "()(())",
 * "()()()"
 * ]
 */
public class GenerateParenthesis_22 {
    public static void main(String[] args) {
        System.out.println("generateParenthesis(3) = " + generateParenthesis(3));
        //System.out.println("generateParenthesis(10) = " + generateParenthesis(10));
    }


    public static List<String> generateParenthesis(int n) {
        List<String> result = new LinkedList<>();

        generateParenthesis(result, n, n, new char[2 * n], 0);

        return result;
    }

    // we decrease the count of left and right parenthesis and add generated strings only when they reached 0,
    // as we never repeat index all strings will be unique
    public static void generateParenthesis(List<String> agg, int left, int right, char[] string, int index) {
        if (left < 0 || right < left) {// there shall never be more right than left
            return;
        }
        else if (left == 0 && right == 0) { // we reached the end and generated a string, so we can add it to result
            agg.add(String.valueOf(string));
        }
        else { // we decrease the count of left and right and recurse
            string[index] = '(';
            generateParenthesis(agg, left - 1, right, string, index + 1);

            string[index] = ')';
            generateParenthesis(agg, left, right - 1, string, index + 1);
        }

    }
}
