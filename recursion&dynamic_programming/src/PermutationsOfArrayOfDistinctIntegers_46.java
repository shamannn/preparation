import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class PermutationsOfArrayOfDistinctIntegers_46 {
    public static void main(String[] args) {
        System.out.println("permute(new int[]{1,2,3}) = " + permute(new int[]{1, 2, 3}));
        System.out.println("permute(new int[]{2,3,4,7}) = " + permute(new int[]{2, 3, 4, 7}));
    }


    public static List<List<Integer>> permute(int[] nums) {
        if (nums == null) {
            return new ArrayList();
        }

        List<List<Integer>> permutations = new LinkedList<>();

        if (nums.length == 1) {
            List<Integer> one = new LinkedList<>();
            one.add(nums[0]);

            permutations.add(one);
            return permutations;
        }

        int first = nums[0];
        List<List<Integer>> subPermutations = permute(Arrays.copyOfRange(nums, 1, nums.length));

        for (int i = 0; i < subPermutations.size(); i++) {
            List<Integer> subPerm = subPermutations.get(i);


            //normal case
            for (int j = 0; j <= subPerm.size(); j++) {
                List<Integer> newPerm = new LinkedList<>();
                for (int k = 0; k <= subPerm.size(); k++) {
                    if (k == j) {
                        newPerm.add(first);
                    }
                    if (k != subPerm.size()) { //adding after all subperms
                        newPerm.add(subPerm.get(k));
                    }
                }
                permutations.add(newPerm);
            }
        }


        return permutations;
    }

}
