import java.util.LinkedList;
import java.util.List;

/**
 * Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.
 * <p>
 * <p>
 * In Pascal's triangle, each number is the sum of the two numbers directly above it.
 * <p>
 * Example:
 * <p>
 * Input: 5
 * Output:
 * [
 * [1],
 * [1,1],
 * [1,2,1],
 * [1,3,3,1],
 * [1,4,6,4,1]
 * ]
 */
public class PascalTriangle_118 {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new LinkedList<>();
        List<Integer> current = new LinkedList<>();

        if (numRows < 1) {
            //result.add(current);

            return result;
        }

        current.add(1); // first number is always 1


        if (numRows == 1) {
            result.add(current);

            return result;
        }
        else {
            List<List<Integer>> previous = generate(numRows - 1);
            result.addAll(previous);

            for (int i = 1; i < numRows - 1; i++) {
                List<Integer> currentPrevious = previous.get(numRows - 2);
                current.add(currentPrevious.get(i - 1) + currentPrevious.get(i));
            }
            current.add(1); // last number is always 1


            result.add(current);
            return result;
        }
    }
}
