public class KnapsackPacking_Barclays {
    public static void main(String[] args) {
        System.out.println("packKnapSack(new int[]{10, 20,30}, new int[]{60, 100, 120}, 50, 3) = " +
                packKnapSack(new int[]{10, 20, 30}, new int[]{60, 100, 120}, 50, 3));
    }

    public static int packKnapSack(int[] weights, int[] values, int maxWeight, int n) {

//        return packKnapSackRecursive(weights, values, maxWeight, n);
        return knapSackDP(weights, values, maxWeight, n);
    }

    private static int knapSackDP(int[] weights, int[] values, int maxWeight, int n) {
        // dynamic programming version
        int[][] dp = new int[n + 1][maxWeight + 1];

        for (int i = 0; i <= n; i++) {
            for (int w = 0; w <= maxWeight; w++) {
                if (i == 0 || w == 0) {
                    dp[i][w] = 0;
                }
                else if (weights[i - 1] > w) {
                    dp[i][w] = dp[i - 1][w];
                }
                else {
                    dp[i][w] = Math.max(values[i - 1] + dp[i - 1][w - weights[i - 1]], dp[i - 1][w]);
                }
            }
        }
        int result = dp[n][maxWeight];

        System.out.println("max result = " + result);

        result = print(weights, values, maxWeight, n, dp, result);

        return result;
    }

    private static int print(int[] weights, int[] values, int maxWeight, int n, int[][] dp, int result) {
        int w = maxWeight;
        for (int i=n; i>0 && result >0; i--){
            if (result != dp[i-1][w]){
                System.out.println("weight="+ weights[i-1] + ", values=" + values[i-1]);

                result -= values[i-1];
                w -= weights[i-1];
            }
        }
        return result;
    }

    private static int packKnapSackRecursive(int[] weights, int[] values, int maxWeight, int n) {
        if (n == 0 || maxWeight == 0) {
            return 0;
        }

        if (weights[n - 1] > maxWeight) { // we can't include this nth item as we exceed maximum weight
            return packKnapSack(weights, values, maxWeight, n - 1); // so we take previous maximum
        }
        else { // we include the maximum between (n-1)th maximum (we don't add current item) and
            return Math.max(
                    packKnapSack(weights, values, maxWeight, n - 1), // case we don't add current item)
                    values[n - 1] + // we take current item, so we calculate add current value
                            packKnapSack(weights, values, maxWeight - weights[n - 1], n - 1));
            // and maximum for n-1 and (maximum weight - current weight)

        }
    }
}
