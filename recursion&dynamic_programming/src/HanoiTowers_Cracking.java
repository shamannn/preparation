import java.util.Stack;

public class HanoiTowers_Cracking {

    public static void main(String[] args) {

        hanoiTowers();
    }

    private static void hanoiTowers() {
        Stack<Integer> first = new Stack();
        Stack<Integer> second = new Stack();
        Stack<Integer> third = new Stack();

        //initial position
        int N = 10;
        for (int i = N; i > 0; i--) {
            first.push(i); // number corresponds to ring size
        }

        System.out.println("first.size() = " + first.size());

        moveDisks(N, first, second, third);

        System.out.println("AFTER");
        print(first, second, third);
    }


    private static void moveDisks(int n, Stack<Integer> from, Stack<Integer> to, Stack<Integer> buffer) {

        if (n > 0) {
            moveDisks(n - 1, from, buffer, to); // n -1
            moveTop(from, to); // last element
            moveDisks(n - 1, buffer, to, from);
        }
    }

    private static void moveTop(Stack<Integer> from, Stack<Integer> to) {
        int top = from.pop();
        to.push(top);
    }

    private static void print(Stack<Integer> first, Stack<Integer> second, Stack<Integer> third) {

        System.out.println("first");
        popAndPrint(first);

        System.out.println("second");

        popAndPrint(second);

        System.out.println("third");

        popAndPrint(third);
    }

    private static void popAndPrint(Stack<Integer> stack) {
        while (!stack.isEmpty()) {
            System.out.println("stack.pop() = " + stack.pop());
        }
    }
}
