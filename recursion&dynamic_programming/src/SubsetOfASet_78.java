import java.util.*;
import java.util.stream.Collectors;

public class SubsetOfASet_78 {
    public static void main(String[] args) {

    }


    public List<List<Integer>> subsets(int[] nums) {
        //I'd rather use set here, but we have a given API and converting will increase time
        if (nums == null) {
            return new ArrayList<>();//empty list
        }


        //List<List<Integer>> agg = new ArrayList<List<Integer>>((int)Math.pow(2, nums.length));

        Set<List<Integer>> agg = new HashSet<List<Integer>>((int) Math.pow(2, nums.length));


        return subsets(nums, agg).stream().collect(Collectors.toList());
    }

    public Set<List<Integer>> subsets(int[] nums, Set<List<Integer>> agg) {
        //public List<List<Integer>> subsets(int[] nums, List<List<Integer>> agg) {
        if (nums.length == 1) {
            List<Integer> subset = new ArrayList<>();
            subset.add(nums[0]);

            agg.add(new ArrayList<>());
            agg.add(subset);
        }
        else {
            int first = nums[0];

            System.out.println(first);

            /*List<Integer> set = new ArrayList<>(1);
            set.add(first);
*/
            agg.add(new ArrayList<>(1));

            Set<List<Integer>> subsets = new HashSet(subsets(Arrays.copyOfRange(nums, 1, nums.length), agg));


            for (List subset : subsets) {
                List<Integer> newSubset = new ArrayList<>(subset);

                newSubset.add(first);

                agg.add(newSubset);
            }

        }

        return agg;
    }
}