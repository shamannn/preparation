/**
 * climbing stairs
 *
 * Soution recursively with memoization (dynamic programming top to bottom).
 *
 * O(2^n) time complexity for recursive solution without memo, O(n) with memo.
 * O(n) space complexity though due to using array, but with pure recursion without memo it fails on n=44.
 */
public class ClimbingStairs_70 {
    public static void main(String[] args) {
        int stairs2 = climbStairs(2);

        System.out.println("stairs2 = " + stairs2);

        int stairs3 = climbStairs(3);

        System.out.println("stairs3 = " + stairs3);

        int stairs4 = climbStairs(4);

        System.out.println("stairs4 = " + stairs4);

        int stairsBig = climbStairs(10000);// with one more 0 stackoverflow

        System.out.println("stairsBig = " + stairsBig);
    }

    public static int climbStairs(int n) {
        return climbStairs(n, new int[n]);
    }

    public static int climbStairs(int n, int[] memo) {
        if (n <= 2) {
            return n;
        }
        else {
            int minus1 = 0;
            int minus2 = 0;

            if (memo[n - 1] == 0) {
                minus1 = climbStairs(n - 1, memo);
                memo[n - 1] = minus1;
            }
            else {
                minus1 = memo[n - 1];
            }

            if (memo[n - 2] == 0) {
                minus2 = climbStairs(n - 2, memo);
                memo[n - 2] = minus2;
            }
            else {
                minus2 = memo[n - 2];
            }

            return minus1 + minus2;

        }
    }
}
