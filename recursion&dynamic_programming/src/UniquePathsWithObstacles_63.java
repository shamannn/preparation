/**
 A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

 The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

 Now consider if some obstacles are added to the grids. How many unique paths would there be?

 An obstacle and empty space is marked as 1 and 0 respectively in the grid.

 Note: m and n will be at most 100.
 */
public class UniquePathsWithObstacles_63 {
    public static void main(String[] args) {
        System.out.println("uniquePaths(3,2) = " + uniquePathsWithObstacles(new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}));
    }

    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid == null) {
            return 0;//??
        }

        int nRows = obstacleGrid.length;
        int nCols = obstacleGrid[0].length;


        int[][] memo = new int[nRows][nCols];


        if (obstacleGrid[0][0] != 1) {//top left corner
            memo[0][0] = 1;
        }

        for (int i = 1; i < nRows; i++) {
            if (obstacleGrid[i][0] != 1 && obstacleGrid[i - 1][0] != 1) {
                memo[i][0] = 1;
            }
            else {
                break;
            }
        }

        for (int i = 1; i < nCols; i++) {
            if (obstacleGrid[0][i] != 1 && obstacleGrid[0][i - 1] != 1) {
                memo[0][i] = 1;
            }
            else {
                break;
            }
        }


        for (int i = 1; i < nRows; i++) {
            for (int j = 1; j < nCols; j++) {
                if (obstacleGrid[i][j] != 1) {
                    if (obstacleGrid[i - 1][j] != 1 && obstacleGrid[i][j - 1] != 1) {
                        memo[i][j] = memo[i - 1][j] + memo[i][j - 1];
                    }
                    else if (obstacleGrid[i - 1][j] != 1 && obstacleGrid[i][j - 1] == 1) {
                        memo[i][j] = memo[i - 1][j];
                    }
                    else if (obstacleGrid[i - 1][j] == 1 && obstacleGrid[i][j - 1] != 1) {
                        memo[i][j] = memo[i][j - 1];
                    }  // else stays 0
                }
            }
        }

        for (int i = 0; i < nRows; i++) {
            for (int j = 0; j < nCols; j++) {
                System.out.print(memo[i][j]);
            }
            System.out.println();
        }

        return memo[nRows - 1][nCols - 1];
    }
}
