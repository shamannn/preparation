import java.util.*;
import java.util.stream.Collectors;

/**
 * Given a collection of numbers that might contain duplicates, return all possible unique permutations.
 */
public class PermutationsOfArrayofNONDistinctIntegers_47 {

    public List<List<Integer>> permuteUnique(int[] nums) {
        if ((nums == null) || (nums.length == 0)) {
            return new LinkedList();// empty list
        }

        Set<List<Integer>> permutes = permute(nums);

        return permutes.stream().collect(Collectors.toList());
    }

    public Set<List<Integer>> permute(int[] nums) {
        if (nums.length == 1) {
            Set<List<Integer>> permuteSet = new HashSet<>();
            List<Integer> permute = new LinkedList<>();
            permute.add(nums[0]);
            permuteSet.add(permute);

            return permuteSet; // single element set
        }
        else {
            int first = nums[0];

            Set<List<Integer>> subPermutes = permute(Arrays.copyOfRange(nums, 1, nums.length));

            Set<List<Integer>> permuteSet = new HashSet<>();

            for (List<Integer> subPermute : subPermutes) {
                insetAtEveryPlace(first, permuteSet, subPermute);
            }

            return permuteSet;
        }
    }

    private void insetAtEveryPlace(int first, Set<List<Integer>> permuteSet, List<Integer> subPermute) {
        for (int i = 0; i <= subPermute.size(); i++) {
            List<Integer> permute = new LinkedList<>();

            for (int j = 0; j <= subPermute.size(); j++) {
                if (i == j) {
                    permute.add(first);
                }
                if (j != subPermute.size()) {
                    permute.add(subPermute.get(j));
                }
            }
            permuteSet.add(permute);
        }
    }
}