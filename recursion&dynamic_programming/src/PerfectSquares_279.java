import java.util.Arrays;

/**
 * Given a positive integer n, find the least number of perfect square numbers (for example, 1, 4, 9, 16, ...) which sum to n.
 * <p>
 * Example 1:
 * <p>
 * Input: n = 12
 * Output: 3
 * Explanation: 12 = 4 + 4 + 4.
 * Example 2:
 * <p>
 * Input: n = 13
 * Output: 2
 * Explanation: 13 = 4 + 9.
 */
public class PerfectSquares_279 {
    public static void main(String[] args) {
        System.out.println("numSquares(48) = " + numSquares(48));
        System.out.println("numSquares(12) = " + numSquares(12));
    }


    /**
     * dp[0] = 0
     * dp[1] = dp[0]+1 = 1
     * dp[2] = dp[1]+1 = 2
     * dp[3] = dp[2]+1 = 3
     * dp[4] = Min{ dp[4-1*1]+1, dp[4-2*2]+1 }
     * = Min{ dp[3]+1, dp[0]+1 }
     * = 1
     * dp[5] = Min{ dp[5-1*1]+1, dp[5-2*2]+1 }
     * = Min{ dp[4]+1, dp[1]+1 }
     * = 2
     * .
     * .
     * .
     * dp[13] = Min{ dp[13-1*1]+1, dp[13-2*2]+1, dp[13-3*3]+1 }
     * = Min{ dp[12]+1, dp[9]+1, dp[4]+1 }
     * = 2
     * .
     * .
     * .
     * dp[n] = Min{ dp[n - i*i] + 1 },  n - i*i >=0 && i >= 1
     *
     *  Time complexity is O(n^1.5), as the inner loop runs till sqrt(n)
     *  Space complexity is O(n)
     */
    public static int numSquares(int n) {
        if (n < 0){
            return -1;
        }

        int[] dp = new int[n+1];
        Arrays.fill(dp, Integer.MAX_VALUE);


        dp[0] = 0;

        for (int i=1; i<=n; i++){

            for (int j=1; j*j<=i; j++){
                dp[i] = Math.min(dp[i], dp[i-j*j] +1);
                // the sum needs to end up with a square of some number less than i, hence "- j*j"
                // and from that position we take minimum number of squares for it (dp[i-j*j])
                // so we choose the minimum of all "paths" leading to i-th number
            }


        }

        return dp[n];
    }
    // ANOTHER solution
//    public static int numSquares(int n) {
//        int[] dp = new int[n + 1];
//        Arrays.fill(dp, Integer.MAX_VALUE);
//        dp[0] = 0;
//        for (int i = 1; i <= n; ++i) {
//            int min = Integer.MAX_VALUE;
//            int j = 1;
//            while (i - j * j >= 0) {
//                min = Math.min(min, dp[i - j * j] + 1);
//                ++j;
//            }
//            dp[i] = min;
//        }
//        return dp[n];
//    }
}
