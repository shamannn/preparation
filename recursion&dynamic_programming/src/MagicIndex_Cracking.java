/**
 * Magic Index: A magic index in an array A[ 1 .•. n-1] is defined to be an index such that A[ i] i.
 * Given a sorted array of distinct integers, write a method to find a magic index, if one exists, in array A.
 * FOLLOW UP What if the values are not distinct?
 * <p>
 * 1 3 6 7 9
 * <p>
 * can it be sorted descending?
 */
public class MagicIndex_Cracking {
    public static void main(String[] args) {
        System.out.println("magicIndex(new int[]{0, 3, 6, 7, 9}) = " + magicIndex(new int[]{0, 3, 6, 7, 9}));
        System.out.println("magicIndex(new int[]{-2, 1, 5, 7, 9}) = " + magicIndex(new int[]{-2, 1, 5, 7, 9}));
        System.out.println("magicIndex(new int[]{2, 3, 6, 7, 9}) = " + magicIndex(new int[]{2, 3, 6, 7, 9}));
        System.out.println("magicIndex(new int[]{3, 2}) = " + magicIndex(new int[]{3, 2}));
        System.out.println("magicIndex(new int[]{3, 2}) = " + magicIndex(new int[]{3, 2, 2}));
        System.out.println("magicIndex(new int[]{3, 3,3,4}) = " + magicIndex(new int[]{3, 3, 3, 4}));

        System.out.println("magicIndex(new int[]{-1, 0,0,4}) = " + magicIndex(new int[]{-1, 0, 0, 4}));

        System.out.println("magicIndex(new int[][]{-10, -5, 2,2,2,3,4,9,12,13}) = "
                + magicIndex(new int[]{-10, -5, 2, 2, 2, 3, 4, 9, 12, 13}));
    }

    public static int magicIndex(int[] nums) {
        return magicIndex(nums, 0, nums.length);
    }

    // let's use something similar to binary search, as O(log N) is better than O(n)
    //!!! doesn't for non-distinct elements
    public static int magicIndex(int[] nums, int startIndex, int endIndex) {
        if (nums == null || nums.length == 0) {
            return -1;
        }

        if (endIndex < startIndex){
            return -1;
        }

        int middleIndex = (startIndex + endIndex) / 2;
        int middle = nums[middleIndex];

        System.out.println("middle = " + middle);
        System.out.println("middleIndex = " + middleIndex);

        if (middle == middleIndex) {
            return middleIndex;
        }

        int leftIndex = Math.min(middleIndex - 1, middle);
        int left = magicIndex(nums, startIndex, leftIndex);

        if (left >= 0) {
            return left;
        }

        int rightIndex = Math.max(middleIndex + 1, middle);
        int right = magicIndex(nums, rightIndex, endIndex);


        return right;
    }

    /** public static int magicIndex(int[] nums) {
     for (int i=0; i<nums.length; i++){
     if(nums[i] == (i + 1)){
     return i+1;
     }
     }

     return -1; // default value?
     }*/
}
