/**
 * Given two strings s and t, determine if they are both one edit distance apart.
 * <p>
 * Note:
 * <p>
 * There are 3 possiblities to satisify one edit distance apart:
 * <p>
 * Insert a character into s to get t
 * Delete a character from s to get t
 * Replace a character of s to get t
 * Example 1:
 * <p>
 * Input: s = "ab", t = "acb"
 * Output: true
 * Explanation: We can insert 'c' into s to get t.
 * Example 2:
 * <p>
 * Input: s = "cab", t = "ad"
 * Output: false
 * Explanation: We cannot get t from s by only one step.
 * Example 3:
 * <p>
 * Input: s = "1203", t = "1213"
 * Output: true
 * Explanation: We can replace '0' with '1' to get t.
 */
public class OneEditDistance_161 {
    public boolean isOneEditDistance(String s, String t) {
        if (s == null || t == null) {
            return false;
        }

        int sLength = s.length(), tLength = t.length();

        if ((sLength == 0 && tLength == 1) || (tLength == 0 && sLength == 1)) {
            return true;
        }

        if (sLength == tLength) {
            int countDiff = 0;

            for (int i = 0; i < sLength; i++) {
                if (s.charAt(i) != t.charAt(i)) {
                    countDiff++;
                }
            }

            return countDiff == 1;
        }
        else if (Math.abs(sLength - tLength) == 1) {
            int countDiff = 0;

            for (int i = 0, j = 0; i < sLength && j < tLength; i++, j++) {
                if (s.charAt(i) != t.charAt(j)) {
                    if (i + 1 < sLength && s.charAt(i + 1) == t.charAt(j)) {
                        countDiff++;
                        i++;
                    }
                    else if (j + 1 < tLength && s.charAt(i) == t.charAt(j + 1)) {
                        countDiff++;
                        j++;
                    }
                    else {
                        return false;
                    }
                }
            }

            return countDiff <= 1;
        }

        return false;
    }
}
