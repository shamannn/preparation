/**
 * Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.
 * <p>
 * Note:
 * <p>
 * The length of both num1 and num2 is < 5100.
 * Both num1 and num2 contains only digits 0-9.
 * Both num1 and num2 does not contain any leading zero.
 * You must not use any built-in BigInteger library or convert the inputs to integer directly.
 */
public class AddStrings_415 {
    public String addStrings(String num1, String num2) {
        if (num1 == null || num2 == null) {
            return null;
        }

        int i = num1.length() - 1, j = num2.length() - 1, carryOver = 0, current, first, second;


        StringBuilder result = new StringBuilder();
        while (i >= 0 || j >= 0) {
            if (i >= 0) {
                first = parse(num1.charAt(i));
            }
            else {
                first = 0;
            }
            if (j >= 0) {
                second = parse(num2.charAt(j));
            }
            else {
                second = 0;
            }
            current = carryOver + first + second;

            result.insert(0, current % 10);

            if (current >= 10) {
                carryOver = 1;
            }
            else {
                carryOver = 0;
            }

            i--;
            j--;
        }

        if (carryOver == 1) {
            result.insert(0, '1');
        }

        return result.toString();
    }

    int parse(char input) {
        switch (input) {
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
        }
        return 0;
    }
}
