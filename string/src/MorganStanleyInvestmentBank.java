public class MorganStanleyInvestmentBank {

    public static void getAcronyms(String[] input, int[] indexes){


        for (int i=0; i < input.length; i++) {
            String nextString = input[i];

            System.out.print(nextString.charAt(indexes[i]));
        }
        System.out.print(",");


        boolean finishProcessing = generateIndexes(input, indexes);

        for (int index : indexes) {
//            System.out.println("index = " + index);
        }

        if (!finishProcessing) {
            getAcronyms(input, indexes);
        }
        // TODO call getAcronyms recursively with increased indexes
    }

    // TODO so indexes like 0000,0001,0002,0003 until last string size, than 0010
        private static boolean generateIndexes(String[] input, int[] indexes) {
        int[] carryOver = new int[input.length];

        boolean finishProcessing = false;
        boolean increaseHappened = false;

        for (int j=indexes.length -1; j >= 0; j--){
  /**          System.out.println("j = " + j);
            System.out.println("carryOver[j] = " + carryOver[j]);
            System.out.println("indexes[j] = " + indexes[j]);
*/
            int nextIndex = indexes[j] + carryOver[j];

           //System.out.println("nextIndex = " + nextIndex);

            if((nextIndex == input[j].length()) && (j == 0)){
               finishProcessing = true;
               break;
            }
            else if((nextIndex == input[j].length() -1) && (j > 0)){ // when we need to carry over one
                carryOver[j-1]++;
                indexes[j] = 0;
            }
            else if (!increaseHappened){
                if (carryOver[j] == 0) {
                    indexes[j] = nextIndex + 1;
                }
                else {
                    indexes[j] = nextIndex;
                    carryOver[j] = 0;
                }
                increaseHappened = true;
            }
        }

        return finishProcessing;
    }

    public static void main(String[] args) {
        getAcronyms(new String[]{"MORGAN","STANLEY","INVESTMENT","BANK"}, new int[]{0,0,0,0});
    }// TODO should return MSIB, MSIA, MSIN, MSIK, MSNB, ....
    // TODO so indexes like 0000,0001,0002,0003 until last string size, than 0010
}
