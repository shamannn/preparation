import java.util.HashSet;
import java.util.Set;

/**
 * Given two strings A and B, find the minimum number of times A has to be repeated such that B is a substring of it. If no such solution, return -1.
 * <p>
 * For example, with A = "abcd" and B = "cdabcdab".
 * <p>
 * Return 3, because by repeating A three times (“abcdabcdabcd”), B is a substring of it; and B is not a substring of A repeated two times ("abcdabcd").
 * <p>
 * Note:
 * The length of A and B will be between 1 and 10000.
 */
public class RepeatedStringMatch_686 {

    public int repeatedStringMatch(String A, String B) {
        if(A == null){
            return 1;
        }
        else if (B == null){
            return -1;// error
        }
        else if (A.contains(B)){
            return 1;// ??
        }
        else if (!hasSameSymbols( A, B)){//if suymbols are different, there is no point in repeating
            return -1;
        }

        int multiplier = 1;

        StringBuilder big = new StringBuilder(A);// reuse the string
        while (multiplier <= Math.max(B.length(), A.length()) ){
            if(big.length() >= B.length() && big.indexOf(B) != -1){
                return multiplier;
            }

            big = big.append(A);
            multiplier++;
        }

        return -1;
    }

    /**
     * chechks if two strings have same symbols
     *
     * */
    private boolean hasSameSymbols(String A, String B){
        Set<Character> symbolsA = new HashSet();

        for(int i=0; i<A.length(); i++){
            symbolsA.add(A.charAt(i));
        }

        for(int j=0; j<B.length(); j++){
            if(!symbolsA.contains(B.charAt(j))){
                return false;
            }
        }


        return true;
    }
}