public class NeedleInHaystack_StrStr_28 {
    public int strStr(String haystack, String needle) {
        if (haystack == null || needle == null || needle.length() > haystack.length()) {
            return -1;
        }
        else if (haystack.length() == 0 || needle.length() == 0) {
            return 0;
        }

        char[] hayArray = haystack.toCharArray();
        char[] needleArray = needle.toCharArray();


        for (int i = 0; i < hayArray.length; i++) {
            if (hayArray[i] == needleArray[0]) {
                boolean flag = true;
                if (hayArray.length - i < needleArray.length) {// not enough space for needle
                    flag = false;
                }
                else {
                    for (int j = 0; j < needleArray.length; j++) {//now check if needle matches
                        if (hayArray[i + j] != needleArray[j]) {
                            flag = false;
                        }
                    }
                }
                if (flag) {
                    return i;
                }
            }
        }

        return -1;
    }
}
