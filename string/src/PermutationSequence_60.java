import java.util.LinkedList;
import java.util.List;

public class PermutationSequence_60 {
    public String getPermutation(int n, int k) {
        if (n <= 0 || k <= 0) {
            return "";
        }

        List<Integer> numbers = new LinkedList<>();
        for (int i = 1; i <= n; i++) { // this list will be decreasing
            numbers.add(i);
        }

        // calculating factorials
        int[] factorials = new int[n];
        factorials[0] = 1;
        for (int i = 1; i < n; i++) {
            factorials[i] = i * factorials[i - 1];
        }

        k--; // as indexes start with 0

        StringBuilder result = new StringBuilder();
        for (int i = n; i > 0; i--) {
            int index = k / factorials[i - 1];
            k %= factorials[i - 1];
            result.append(numbers.get(index));
            numbers.remove(index);
        }

        return result.toString();
    }

}