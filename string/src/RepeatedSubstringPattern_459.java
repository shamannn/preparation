/**
 * Given a non-empty string check if it can be constructed by taking a substring of it and appending multiple copies of the substring together. You may assume the given string consists of lowercase English letters only and its length will not exceed 10000.
 * Example 1:
 * Input: "abab"
 * <p>
 * Output: True
 * <p>
 * Explanation: It's the substring "ab" twice.
 * Example 2:
 * Input: "aba"
 * <p>
 * Output: False
 * Example 3:
 * Input: "abcabcabcabc"
 * <p>
 * Output: True
 * <p>
 * Explanation: It's the substring "abc" four times. (And the substring "abcabc" twice.)
 */
public class RepeatedSubstringPattern_459 {
    // this solution is O(n^2)
    public boolean repeatedSubstringPattern(String s) {

        int len = s.length();

        for (int subLength = 1; subLength <= len / 2; subLength++) {

            boolean thisOne = true;
            //check if it's a substring
            if (len % subLength != 0) {
                thisOne = false;
            }
            else {
                for (int j = 0, k = 0; j < len && k < subLength; j++, k = j % subLength) {
                    if (s.charAt(j) != s.charAt(k)) {
                        thisOne = false;
                        break;
                    }
                }
            }
            if (thisOne) {
                return true;
            }
        }

        return false;
    }
}