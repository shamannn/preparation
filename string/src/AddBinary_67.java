/**
 * Given two binary strings, return their sum (also a binary string).
 * <p>
 * The input strings are both non-empty and contains only characters 1 or 0.
 * <p>
 * Example 1:
 * <p>
 * Input: a = "11", b = "1"
 * Output: "100"
 * Example 2:
 * <p>
 * Input: a = "1010", b = "1011"
 * Output: "10101"
 */
public class AddBinary_67 {
    public String addBinary(String a, String b) {

        char[] left;
        char[] right;

        if (a.length() > b.length()) {
            left = a.toCharArray();
            right = b.toCharArray();
        }
        else {
            left = b.toCharArray();
            right = a.toCharArray();
        }

        StringBuilder result = new StringBuilder();


        int carryOver = 0, current, rightCurrent, leftCurrent;
        for (int i = left.length - 1, j = right.length - 1; i >= 0 || j >= 0; i--, j--) {
            if (j >= 0) {
                rightCurrent = (right[j] == '1') ? 1 : 0;
            }
            else {
                rightCurrent = 0;
            }
            leftCurrent = (left[i] == '1') ? 1 : 0;

            current = leftCurrent + rightCurrent + carryOver;

            // System.out.println(current);

            result.insert(0, current % 2);

            if (current > 1) {
                carryOver = 1;
            }
            else {
                carryOver = 0;
            }
        }

        if (carryOver == 1) {
            result.insert(0, "1");
        }


        return result.toString();
    }
}
