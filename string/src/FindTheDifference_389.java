/**
 * Given two strings s and t which consist of only lowercase letters.
 * <p>
 * String t is generated by random shuffling string s and then add one more letter at a random position.
 * <p>
 * Find the letter that was added in t.
 * <p>
 * Example:
 * <p>
 * Input:
 * s = "abcd"
 * t = "abcde"
 * <p>
 * Output:
 * e
 * <p>
 * Explanation:
 * 'e' is the letter that was added.
 */
public class FindTheDifference_389 {
    /**
     * Simple solution using XOR - same elements will be removed when you XOR them with each other.
     */
    public char findTheDifference(String s, String t) {
        if (s == null || t == null) {
            return ' ';
        }
        else if (s.length() == 0) {
            return t.charAt(0);
        }
        char[] sArray = s.toCharArray();
        char[] tArray = t.toCharArray();

        char result = sArray[0];

        for (int j = 1; j < sArray.length; j++) {
            result ^= sArray[j];
        }


        for (int i = 0; i < tArray.length; i++) {
            result ^= tArray[i];
        }

        return result;
    }
}

