import java.util.LinkedList;
import java.util.List;

public class ReverseWordsInString_151 {
    public static void main(String[] args) {
        String sky = reverseWords("the sky is blue");
        System.out.println("sky = " + sky);

        String numbers = reverseWords("one two three");
        System.out.println("numbers = " + numbers);

    }

    private static String reverseWords(String s) {
        if ((s == null) || (s.length() == 0)){
            return "";
        }

        List<String> wordsList = new LinkedList<>();

        int index = 0;
        while (index < s.length()){

            if(s.charAt(index) != ' '){
                StringBuilder stringBuilder = new StringBuilder();

                while ((index < s.length()) && (s.charAt(index) != ' ')){
                    stringBuilder.append(s.charAt(index));
                    index++;
                }
                wordsList.add(stringBuilder.toString());
            }
            else{
                index++;
            }
        }


        StringBuilder wordBuilder = new StringBuilder();
        for (int i = wordsList.size() - 1; i >= 0; i--) {
            String word = wordsList.get(i).trim();

            wordBuilder.append(word);

            if (i > 0){
                wordBuilder.append(" ");
            }
        }


        return wordBuilder.toString();
    }
}
