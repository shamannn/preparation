import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * A string S of lowercase letters is given. We want to partition this string into as many parts as possible so that each letter appears in at most one part, and return a list of integers representing the size of these parts.
 * <p>
 * Example 1:
 * Input: S = "ababcbacadefegdehijhklij"
 * Output: [9,7,8]
 * Explanation:
 * The partition is "ababcbaca", "defegde", "hijhklij".
 * This is a partition so that each letter appears in at most one part.
 * A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it splits S into less parts.
 * Note:
 * <p>
 * S will have length in range [1, 500].
 * S will consist of lowercase letters ('a' to 'z') only.
 */
public class PartitionLabels_763 {
    public List<Integer> partitionLabels(String S) {
        if (S == null) {
            return new LinkedList<>();
        }

        int[] lettersFirst = new int[26];
        int[] lettersLast = new int[26];
        Arrays.fill(lettersFirst, -1);
        Arrays.fill(lettersLast, -1);


        char[] input = S.toCharArray();

        for (int i = 0; i < input.length; i++) {
            char current = input[i];

            if (lettersFirst[current - 'a'] == -1) {
                lettersFirst[current - 'a'] = i;
            }

            lettersLast[current - 'a'] = i;
        }

        List<Integer> result = new LinkedList<>();

        int partitionLen = 0;
        int currentIndex = 0;

        while (currentIndex < input.length) {
            char current = input[currentIndex];

            int start = lettersFirst[current - 'a'];
            int end = lettersLast[current - 'a'];
            partitionLen = end - start;


            for (int j = start; j <= end; j++) {
                char next = input[j];

                if (lettersLast[next - 'a'] > end) {
                    end = lettersLast[next - 'a'];  // moving end
                    partitionLen = (end - start);
                }
            }

            result.add(++partitionLen);
            currentIndex += partitionLen;
        }

        return result;
    }
}
