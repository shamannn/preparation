public class IsPalyndrome_125 {

    public static void main(String[] args) {
        System.out.println("isPalindrome(\"trete\") = " + isPalindrome("trete"));
        System.out.println("isPalindrome(\"A man, a plan, a canal: Panama\") = " + isPalindrome("A man, a plan, a canal: Panama"));
    }

    public static boolean isPalindrome(String s) {
        if (s == null) {
            return false;
        }
        else if (s.length() == 0) {
            return true;
        }

        // removing non-alphanumeric characters
        String replaced = s.replaceAll("[^A-Za-z0-9]", "").toLowerCase();// will remove non Latin symbols as well

        char[] chars = replaced.toCharArray();

        if (replaced.length() % 2 == 0) { // even length
            int leftIndex = replaced.length() / 2 - 1;
            int rightIndex = replaced.length() / 2;

            return iterateThroughHalfString(replaced, chars, leftIndex, rightIndex);
        }
        else {// odd length
            int leftIndex = (replaced.length() - 1) / 2 - 1;
            int rightIndex = (replaced.length() + 1) / 2;

            return iterateThroughHalfString(replaced, chars, leftIndex, rightIndex);
        }

    }

    private static boolean iterateThroughHalfString(String replaced, char[] chars, int leftIndex, int rightIndex) {
        while (leftIndex >= 0 && rightIndex < replaced.length()) {

            if (chars[leftIndex] != chars[rightIndex]) {
                return false;
            }

            leftIndex--;
            rightIndex++;
        }
        return true;
    }
}

