import java.util.HashMap;
import java.util.Map;


/**
 * Implement a trie with insert, search, and startsWith methods.
 * <p>
 * Example:
 * <p>
 * Trie trie = new Trie();
 * <p>
 * trie.insert("apple");
 * trie.search("apple");   // returns true
 * trie.search("app");     // returns false
 * trie.startsWith("app"); // returns true
 * trie.insert("app");
 * trie.search("app");     // returns true
 * Note:
 * <p>
 * You may assume that all inputs are consist of lowercase letters a-z.
 * All inputs are guaranteed to be non-empty strings.
 */
public class ImplementTrie_208 {
    class Trie {
        TrieNode root; // do we need root node?

        /**
         * Initialize your data structure here.
         */
        public Trie() {
            root = new TrieNode((char) 0); // some special character
        }

        /**
         * Inserts a word into the trie.
         */
        public void insert(String word) {
            if (word == null) {
                return;
            }

            Map<Character, TrieNode> currentChildren = root.children;

            for (char letter : word.toCharArray()) {
                TrieNode current = currentChildren.get(letter);

                if (current != null) {
                    currentChildren = current.children;
                }
                else {
                    TrieNode newNode = new TrieNode(letter);
                    currentChildren.put(letter, newNode); // adding this letter
                    currentChildren = newNode.children;
                }
            }

            currentChildren.put((char) 0, new EndNode()); // finishing the word
        }

        /**
         * Returns if the word is in the trie.
         */
        public boolean search(String word) {
            if (word == null) {
                return false;
            }

            Map<Character, TrieNode> currentChildren = root.children;

            for (char letter : word.toCharArray()) {
                TrieNode current = currentChildren.get(letter);

                if (currentChildren.isEmpty() || current == null) {
                    return false;
                }
                currentChildren = current.children;
            }

            return currentChildren.get((char) 0) instanceof EndNode;
        }

        /**
         * Returns if there is any word in the trie that starts with the given prefix.
         */
        public boolean startsWith(String prefix) {
            if (prefix == null) {
                return true;    // ??
            }

            Map<Character, TrieNode> currentChildren = root.children;

            for (char letter : prefix.toCharArray()) {
                TrieNode current = currentChildren.get(letter);

                if (currentChildren.isEmpty() || current == null) {
                    return false;
                }
                currentChildren = current.children;
            }

            return true;
        }
    }

    class TrieNode {
        char letter;
        Map<Character, TrieNode> children;

        public TrieNode(char letter) {
            this.children = new HashMap<>();
            this.letter = letter;
        }
    }

    class EndNode extends TrieNode {
        public EndNode() {
            super((char) 0);
        }
    }
}

/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * boolean param_2 = obj.search(word);
 * boolean param_3 = obj.startsWith(prefix);
 */
