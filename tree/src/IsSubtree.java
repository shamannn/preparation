/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class IsSubtree {
    public static void main(String[] args) {
        boolean isSubtree = new IsSubtree().isSubtree(null, null);

        System.out.println("null isSubtree = " + isSubtree);


        isSubtree = new IsSubtree().isSubtree(null, new TreeNode(3));

        System.out.println("isSubtree = " + isSubtree);
    }

    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s!= null && t!= null){
            boolean flag = isSameTree(s, t);

            return flag || isSubtree(s.left, t) || isSubtree(s.right, t);
        }
        else if (s == null){
            return false;
        }
        else {
            return true;
        }
    }

    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p != null && q != null) {
            boolean flag = (p.val == q.val);

            return flag && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        }
        else if (p == null && q == null) {
            return true;
        }
        else {
            return false;
        }
    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}