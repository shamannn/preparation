import java.util.LinkedList;
import java.util.List;

public class BinaryTreeLevelOrderTraversal_102_andCracking {

    public static void main(String[] args) {
        System.out.println("levelOrder(null) = " + levelOrder(null));
        System.out.println("levelOrder(new TreeNode()) = " + levelOrder(new TreeNode(1)));
    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new LinkedList<List<Integer>>();


        List<TreeNode> current = new LinkedList<>();
        List<Integer> intList = new LinkedList<>();

        if (root != null) {
            current.add(root);
            intList.add(root.val);
        }


        while (!current.isEmpty()) {
            result.add(intList);

            List<TreeNode> previous = current; // the key point!
            current = new LinkedList<>();
            intList = new LinkedList<>();


            for (TreeNode node : previous) {
                /*System.out.println(node);
                if (node != null) {
                    System.out.println(node.val);
                }*/


                if (node.left != null) {
                    current.add(node.left);
                    intList.add(node.left.val);
                }
                if (node.right != null) {
                    current.add(node.right);
                    intList.add(node.right.val);
                }
            }


        }

        return result;
    }

}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}
