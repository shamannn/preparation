import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Given a binary tree, return the inorder traversal of its nodes' values.

 Example:

 Input: [1,null,2,3]
 1
 \
 2
 /
 3

 Output: [1,3,2]
 Follow up: Recursive solution is trivial, could you do it iteratively?
 */
public class BinaryTreeInOrderTraversal_94 {

    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new LinkedList<>();

        if (root == null){
            return result;
        }


        Stack<TreeNode> stack = new Stack<>();
        //stack.push(root);

        //TreeNode current = root.left;

        while(!stack.isEmpty() || root != null){

            while(root != null){
                stack.push(root);

                root = root.left;
            }

            root = stack.pop();
            result.add(root.val);


            root = root.right;
        }

        return result;
    }
}
