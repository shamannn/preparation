/**
 * Given a binary tree, determine if it is a valid binary search tree (BST).
 * <p>
 * Assume a BST is defined as follows:
 * <p>
 * The left subtree of a node contains only nodes with keys less than the node's key.
 * The right subtree of a node contains only nodes with keys greater than the node's key.
 * Both the left and right subtrees must also be binary search trees.
 */
public class ValidateBinarySearchTree_98_Cracking {
    public static void main(String[] args) {

    }

    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        else {
            //System.out.println("THE root.val = " + root.val);

            return isValidBST(root.left, root.val, true) && isValidBST(root.right, root.val, false)
                    && isValidBST(root.left) && isValidBST(root.right);
        }
    }

    public boolean isValidBST(TreeNode root, int value, boolean less) {
        if (root == null) {
            return true;
        }
          /*
        System.out.println("root.val = " + root.val);
        System.out.println("less = " + less);
        System.out.println("value = " + value);*/


        if (less && root.val >= value) {
            return false;
        }
        else if (!less && root.val <= value) {
            return false;
        }
        else {
            return isValidBST(root.left, value, less) && isValidBST(root.right, value, less);
        }
    }
}


