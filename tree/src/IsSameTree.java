public class IsSameTree {
    public static void main(String[] args) {
        boolean sameTree = new IsSameTree().isSameTree(null, null);

        System.out.println("sameTree = " + sameTree);
    }

    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p != null && q != null) {
            boolean flag = (p.val == q.val);

            return flag && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        }
        else if (p == null && q == null) {
            return true;
        }
        else {
            return false;
        }
    }

    private class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
