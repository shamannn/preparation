import java.util.LinkedList;

/**
 * BST Sequences: A binary search tree was created by traversing through an array
 * from left to right and inserting each element. Given a binary search tree with
 * distinct elements, print all possible arrays that could have led to this tree.
 * EXAMPLE Input:
 * Output: {2, 1, 3}, {2, 3, 1}
 */
public class BstSequences_Cracking_4_9 {

    public LinkedList<LinkedList<Integer>> allSequences(TreeNode node) {
        LinkedList<LinkedList<Integer>> result = new LinkedList<>();

        if (node == null) {
            result.add(new LinkedList<Integer>());
            return result;
        }

        LinkedList<Integer> prefix = new LinkedList<>();
        prefix.add(node.val);

        /* Recurse on left and right subtrees. */
        LinkedList<LinkedList<Integer>> leftSeq = allSequences(node.left);
        LinkedList<LinkedList<Integer>> rightSeq = allSequences(node.right);

        /* Weave together each list from the left and right sides. */
        for (LinkedList<Integer> left : leftSeq) {
            for (LinkedList<Integer> right : rightSeq) {
                LinkedList<LinkedList<Integer>> weaved = new LinkedList<>();

                weaveLists(left, right, weaved, prefix);

                result.addAll(weaved);
            }
        }

        return result;
    }

    /* Weave lists together in all possible ways. This algorithm works by removing the
    *  head from one list, recursing, and then doing the same thing with the other list. */
    void weaveLists(LinkedList<Integer> first, LinkedList<Integer> second, LinkedList<LinkedList<Integer>> results, LinkedList<Integer> prefix) {
        /* One list is empty. Add remainder to [a cloned] prefix and store result. */
        if (first.size() == 0 || second.size() == 0) {
            LinkedList<Integer> clone = (LinkedList<Integer>) prefix.clone();
            clone.addAll(first);
            clone.addAll(second);
            results.add(clone);

            return;
        }

        /* Recurse with head of first added to the prefix. Removing the head will damage
        * first, so we'll need to put it back where we found it afterwards. */
        int headFirst = first.removeFirst();
        prefix.addLast(headFirst);
        weaveLists(first, second, results, prefix);
        first.addLast(headFirst);

        /*same for second*/
        int headSecond = second.removeFirst();
        prefix.addLast(headSecond);
        weaveLists(first, second, results, prefix);
        second.addLast(headSecond);
    }

}

