/**
 * Reverse a singly linked list.
 * <p>
 * Example:
 * <p>
 * Input: 1->2->3->4->5->NULL
 * Output: 5->4->3->2->1->NULL
 * Follow up:
 * <p>
 * A linked list can be reversed either iteratively or recursively. Could you implement both?
 */
public class ReverseList_206 {
    public ListNode reverseList(ListNode head) {

        return reverseListRec(head, null);
    }

    public ListNode reverseListRec(ListNode head, ListNode newHead) {
        if (head == null) { // end of list
            return newHead;
        }

        ListNode tail = head.next;
        head.next = newHead;

        return reverseListRec(tail, head);
    }
}
