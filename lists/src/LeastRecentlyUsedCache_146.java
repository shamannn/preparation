import java.util.HashMap;
import java.util.Map;

/**
 * Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.
 * <p>
 * get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
 * put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
 * <p>
 * Follow up:
 * Could you do both operations in O(1) time complexity?
 * <p>
 * Example:
 * <p>
 * LRUCache cache = new LRUCache( 2 -- capacity);
 * <p>
 * cache.put(1,1);
 * cache.put(2,2);
 * cache.get(1);       // returns 1
 * cache.put(3,3);    // evicts key 2
 * cache.get(2);       // returns -1 (not found)
 * cache.put(4,4);    // evicts key 1
 * cache.get(1);       // returns -1 (not found)
 * cache.get(3);       // returns 3
 * cache.get(4);       // returns 4
 */

public class LeastRecentlyUsedCache_146 {
    class LRUCache {
        private int capacity = 0, count = 0;
        private Map<Integer, Node> map;
        Node head, tail;

        public LRUCache(int capacity) {
            this.capacity = capacity;
            this.map = new HashMap<>();
            head = new Node(0, 0);
            tail = new Node(0, 0);
            head.next = tail;
            tail.previous = head;
        }

        private void deleteNode(Node node) {
            Node previous = node.previous;
            Node next = node.next;
            previous.next = next;
            next.previous = previous;
        }

        private void addToHead(Node node) {
            node.next = head.next;
            node.next.previous = node;
            head.next = node;
            node.previous = head;
        }

        public int get(int key) {
            Node current = map.get(key);

            if (current != null) {
                deleteNode(current);
                addToHead(current); // as it was used we move the node to the head of the linked list

                return current.value;
            }

            return -1;
        }

        public void put(int key, int value) {
            if (map.get(key) != null) { // such key exists - we replace value
                Node current = map.get(key);
                current.value = value;

                deleteNode(current);
                addToHead(current);// as it was used we move the node to the head of the linked list
            }
            else {
                Node newNode = new Node(key, value);
                map.put(key, newNode);

                if (capacity == count) { // we're at our capacity - need to replace node in cache
                    map.remove(tail.previous.key);
                    deleteNode(tail.previous);
                    addToHead(newNode);
                }
                else { // we haven't reached capacity yet - we can just add node
                    addToHead(newNode);
                    count++;
                }
            }
        }

        // double linked list
        class Node {
            int key;
            int value;
            Node previous;
            Node next;

            public Node(int key, int value) {
                this.key = key;
                this.value = value;
            }
        }
    }

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
}
