/**
 * Given a singly linked list, determine if it is a palindrome.
 * <p>
 * Example 1:
 * <p>
 * Input: 1->2
 * Output: false
 * Example 2:
 * <p>
 * Input: 1->2->2->1
 * Output: true
 * Follow up:
 * Could you do it in O(n) time and O(1) space?
 */
public class PalindromeLinkedList_234 {
    public boolean isPalindrome(ListNode head) {
        if (head == null) {
            return true; // null is not palindrome
        }

        // find length
        int len = 0;
        ListNode current = head;
        while (current != null) {
            len++;
            current = current.next;
        }

        if (len == 1) {
            return true;
        }

        // move one pointer to second half
        ListNode halfHead = head;

        for (int i = 0; i < len / 2; i++) {
            halfHead = halfHead.next;
        }
        if (len % 2 != 0) {
            halfHead = halfHead.next;
        }

        // reverse first half
        current = head;
        ListNode previous = null;
        ListNode next = head.next;


        for (int i = 0; i < len / 2; i++) {
            current.next = previous;
            //move
            previous = current;
            current = next;
            next = current.next;
        }

        current = previous;
        // comparing halves
        while ((current != null) && (halfHead != null)) {
            if (current.val != halfHead.val) {
                return false;
            }
            current = current.next;
            halfHead = halfHead.next;
        }

        return true;
    }
}
