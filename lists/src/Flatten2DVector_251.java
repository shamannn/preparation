import java.util.Iterator;
import java.util.List;

/**
 * Implement an iterator to flatten a 2d vector.
 * <p>
 * Example:
 * <p>
 * Input: 2d vector =
 * [
 * [1,2],
 * [3],
 * [4,5,6]
 * ]
 * Output: [1,2,3,4,5,6]
 * Explanation: By calling next repeatedly until hasNext returns false,
 * the order of elements returned by next should be: [1,2,3,4,5,6].
 * Follow up:
 * As an added challenge, try to code it using only iterators in C++ or iterators in Java.
 */
public class Flatten2DVector_251 {
    public class Vector2D implements Iterator<Integer> {
        private Iterator<List<Integer>> listIterator;
        private Iterator<Integer> iterator;


        public Vector2D(List<List<Integer>> vec2d) {
            if (vec2d != null) {
                this.listIterator = vec2d.listIterator();
            }
        }

        @Override
        public Integer next() {
            hasNext();
            return this.iterator.next();
        }

        @Override
        public boolean hasNext() {
            while ((this.iterator == null || !this.iterator.hasNext()) && this.listIterator.hasNext()) {
                this.iterator = this.listIterator.next().iterator();
            }

            return this.iterator != null && this.iterator.hasNext();
        }
    }
}
/**
 * Your Vector2D object will be instantiated and called as such:
 * Vector2D i = new Vector2D(vec2d);
 * while (i.hasNext()) v[f()] = i.next();
 */
// the next solution is faster but uses extra space
/*

public class Vector2D {
    int indexList, indexEle;
    List<List<Integer>> vec;

    public Vector2D(List<List<Integer>> vec2d) {
        indexList = 0;
        indexEle = 0;
        vec = vec2d;
    }

    public int next() {
        return vec.get(indexList).get(indexEle++);
    }

    public boolean hasNext() {
        while(indexList < vec.size()){
            if(indexEle < vec.get(indexList).size())
                return true;
            else{
                indexList++;
                indexEle = 0;
            }
        }
        return false;
    }
}
 */