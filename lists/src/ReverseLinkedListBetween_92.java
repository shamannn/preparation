/**
 * Reverse a linked list from position m to n. Do it in one-pass.
 * <p>
 * Note: 1 ≤ m ≤ n ≤ length of list.
 * <p>
 * Example:
 * <p>
 * Input: 1->2->3->4->5->NULL, m = 2, n = 4
 * Output: 1->4->3->2->5->NULL
 */
public class ReverseLinkedListBetween_92 {
    public ListNode reverseBetween(ListNode head, int m, int n) {
        if (head == null || m < 0 || n < 0) {
            return null;
        }

        ListNode dummy = new ListNode(0);
        dummy.next = head;

        int counter = 1;
        ListNode previous = dummy;
        // move to start of reverse
        while (counter < m) {
            previous = previous.next;

            counter++;
        }

        ListNode current = previous.next;
        ListNode next = current.next;


        // reverse
        while (current != null && counter < n) {
            // reversing
            current.next = next.next;
            next.next = previous.next;
            previous.next = next;

            // moving further
            next = current.next;
            counter++;
        }

        return dummy.next;
    }
}
