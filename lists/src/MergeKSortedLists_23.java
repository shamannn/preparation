import java.util.PriorityQueue;

/**
 * Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.
 * <p>
 * Example:
 * <p>
 * Input:
 * [
 * 1->4->5,
 * 1->3->4,
 * 2->6
 * ]
 * Output: 1->1->2->3->4->4->5->6
 */
public class MergeKSortedLists_23 {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }

        PriorityQueue<ListNode> q = new PriorityQueue<>(lists.length, (ListNode first, ListNode second) -> first.val - second.val);

        for (ListNode node : lists) { // adds first elements of each sorted list to min Heap
            if (node != null) {
                q.add(node);
            }
        }

        //System.out.println(q.size());

        ListNode head = new ListNode(0);
        ListNode tail = head;

        while (!q.isEmpty()) {
            tail.next = q.poll();
            tail = tail.next; // moves tail to next element in the sorted list which was polled from the min Heap
            // which means it's the minimum element at the moment

            if (tail.next != null) { // if current polled list is not finished yet
                q.add(tail.next); //  add it's current element to the min Heap which will put it to the right place
            }
        }

        return head.next;
    }
}
