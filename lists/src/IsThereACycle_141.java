/**
 * Given a linked list, determine if it has a cycle in it.
 * <p>
 * Follow up:
 * Can you solve it without using extra space?
 */
public class IsThereACycle_141 {
    public static void main(String[] args) {
        System.out.println("hasCycle(null) = " + hasCycle(null));
        System.out.println("hasCycle(new ListNode(1)) = " + hasCycle(new ListNode(1)));
    }

    public static boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }

        ListNode slow = head, fast = head.next;


        while (slow != null && fast != null) {

            if (slow == fast) {
                return true;
            }

            if (fast.next != null) {
                fast = fast.next.next;
            }
            else {
                return false;
            }
            slow = slow.next;
        }

        return false;
    }
}
