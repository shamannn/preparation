
/**
 * Write a function to delete a node (except the tail) in a singly linked list, given only access to that node.
 * <p>
 * Supposed the linked list is 1 -> 2 -> 3 -> 4 and you are given the third node with value 3,
 * the linked list should become 1 -> 2 -> 4 after calling your function.
 */
public class DeleteNodeInASingleLinkedList_237 {

    public void deleteNode(MergeTwoSortedLists_21.ListNode node) {
        if ((node != null) && (node.next != null)) {
            MergeTwoSortedLists_21.ListNode next = node.next;
            node.val = next.val;
            node.next = next.next;
        }

    }
}
