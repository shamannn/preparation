/**
 * Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
 * <p>
 * Note: Do not modify the linked list.
 * <p>
 * Follow up:
 * Can you solve it without using extra space?
 */
public class CycleStart_142 {

    // This solution needs a picture!
    public ListNode detectCycle(ListNode head) {
        if (head == null || head.next == null) {
            return null;
        }


        ListNode slow = head, fast = head;

        // find collision point
        while (slow != null && fast != null) {

            slow = slow.next;
            if (fast.next != null) {
                fast = fast.next.next;
            }
            else {
                return null;
            }


            if (slow == fast) {
                break;
            }
        }

        if (slow != fast) { // no cycle
            return null;
        }

        // move to start of cycle
        fast = head;

        while (slow != null && fast != null) {

            if (slow == fast) {
                return slow;
            }


            slow = slow.next;
            fast = fast.next;
        }

        return null;

    }

}
