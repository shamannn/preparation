/**
 * Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.
 * <p>
 * You should preserve the original relative order of the nodes in each of the two partitions.
 * <p>
 * Example:
 * <p>
 * Input: head = 1->4->3->2->5->2, x = 3
 * Output: 1->2->2->4->3->5
 */
public class PartitionList_86 {

    public ListNode partition(ListNode head, int x) {
        // we create two lists - less and greater than x, and later merge them
        ListNode afterHead = null, afterTail = null;
        ListNode beforeHead = null, beforeTail = null;

        while (head != null) {
            ListNode next = head.next; // keep current node
            head.next = null;//! important not to take remains of initial list into before/after

            if (head.val < x) {
                if (beforeHead == null) {
                    beforeHead = head;
                    beforeTail = beforeHead;
                }
                else {
                    beforeTail.next = head;//next points to itself so when we move we keep the new list
                    beforeTail = head;
                }
            }
            else {// adding to "greater" list
                if (afterHead == null) {
                    afterHead = head;
                    afterTail = afterHead;
                }
                else {
                    afterTail.next = head;
                    afterTail = head;
                }
            }

            head = next;
        }

        if (beforeHead == null) {
            return afterHead;
        }

        //merge
        beforeTail.next = afterHead;

        return beforeHead;
    }
}
