import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MergeIntervals_56 {
    public List<Interval> merge(List<Interval> intervals) {
        if ((intervals == null) || (intervals.size() == 0)) {
            return new LinkedList();
        }
        if (intervals.size() == 1) {
            return intervals;
        }

        Collections.sort(intervals, (left, right) -> left.start - right.start); // comparator

        LinkedList<Interval> result = new LinkedList<>();

        int start = intervals.get(0).start;
        int end = intervals.get(0).start;

        for (Interval interval : intervals) {
            if (interval.start <= end) {
                end = Math.max(interval.end, end);
            }
            else {
                result.add(new Interval(start, end));
                start = interval.start;
                end = interval.end;
            }
        }

        result.add(new Interval(start, end));

        return result;
    }


    public class Interval {
        int start;
        int end;

        Interval() {
            start = 0;
            end = 0;
        }

        Interval(int s, int e) {
            start = s;
            end = e;
        }
    }
}
