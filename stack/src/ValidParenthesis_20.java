import java.util.Stack;

public class ValidParenthesis_20 {
    public boolean isValid(String s) {
        if (s == null) {
            return false;
        }

        Stack<Character> stack = new Stack<>();

        boolean flag = true;

        for (int i = 0; i < s.length(); i++) {
            char symbol = s.charAt(i);

            // there are essentially two cases we are interested in -
            // opening parenthesis and ...
            if ((symbol == '(') || (symbol == '{') || (symbol == '[')) {
                stack.push(symbol);
            } // ... closing parenthesis
            else if (symbol == ')') {
                if (stack.isEmpty()) {
                    flag = false;
                    break;
                }
                else {
                    char popped = stack.pop();
                    if (popped != '(') {
                        flag = false;
                        break;
                    }
                }
            }
            else if (symbol == '}') {
                if (stack.isEmpty()) {
                    flag = false;
                    break;
                }
                else {
                    char popped = stack.pop();
                    if (popped != '{') {
                        flag = false;
                        break;
                    }
                }
            }
            else if (symbol == ']') {
                if (stack.isEmpty()) {
                    flag = false;
                    break;
                }
                else {
                    char popped = stack.pop();
                    if (popped != '[') {
                        flag = false;
                        break;
                    }
                }
            }
        }

        return flag && stack.isEmpty();
    }
}
