import java.util.*;

/**
 * A city's skyline is the outer contour of the silhouette formed by all the buildings in that city when viewed from a distance. Now suppose you are given the locations and height of all the buildings as shown on a cityscape photo (Figure A), write a program to output the skyline formed by these buildings collectively (Figure B).
 * <p>
 * Buildings  Skyline Contour
 * The geometric information of each building is represented by a triplet of integers [Li, Ri, Hi], where Li and Ri are the x coordinates of the left and right edge of the ith building, respectively, and Hi is its height. It is guaranteed that 0 ≤ Li, Ri ≤ INT_MAX, 0 < Hi ≤ INT_MAX, and Ri - Li > 0. You may assume all buildings are perfect rectangles grounded on an absolutely flat surface at height 0.
 * <p>
 * For instance, the dimensions of all buildings in Figure A are recorded as: [ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ] .
 * <p>
 * The output is a list of "key points" (red dots in Figure B) in the format of [ [x1,y1], [x2, y2], [x3, y3], ... ] that uniquely defines a skyline. A key point is the left endpoint of a horizontal line segment. Note that the last key point, where the rightmost building ends, is merely used to mark the termination of the skyline, and always has zero height. Also, the ground in between any two adjacent buildings should be considered part of the skyline contour.
 * <p>
 * For instance, the skyline in Figure B should be represented as:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ].
 * <p>
 * Notes:
 * <p>
 * The number of buildings in any input list is guaranteed to be in the range [0, 10000].
 * The input list is already sorted in ascending order by the left x position Li.
 * The output list must be sorted by the x position.
 * There must be no consecutive horizontal lines of equal height in the output skyline. For instance, [...[2 3], [4 5], [7 5], [11 5], [12 7]...] is not acceptable; the three lines of height 5 should be merged into one in the final output as such: [...[2 3], [4 5], [12 7], ...]l
 */
public class SkylineProblem_218 {

    class BuildingList implements Comparable<BuildingList> {
        int x;
        int h;
        boolean isStart;

        @Override
        public int compareTo(BuildingList o) {
            if (this.x != o.x) {
                return this.x - o.x;
            }
            else {
                return ((this.isStart) ? (-this.h) : (this.h)) - ((o.isStart) ? (-o.h) : o.h);
            }
        }
    }

    public List<int[]> getSkyline(int[][] buildings) {
        BuildingList[] blist = new BuildingList[buildings.length * 2];
        for (int i = 0, j = 0; i < buildings.length && j < blist.length; i++, j += 2) {
            blist[j] = new BuildingList();
            blist[j].x = buildings[i][0];
            blist[j].isStart = true;
            blist[j].h = buildings[i][2];

            blist[j + 1] = new BuildingList();
            blist[j + 1].x = buildings[i][1];
            blist[j + 1].isStart = false;
            blist[j + 1].h = buildings[i][2];
        }

        Arrays.sort(blist);

        SortedMap<Integer, Integer> map = new TreeMap<Integer, Integer>();
        List<int[]> result = new ArrayList<int[]>();
        map.put(0, 1);
        int maxHeight = 0, curHeight = 0;

        for (int i = 0; i < blist.length; i++) {
            BuildingList cur = blist[i];

            if (cur.isStart) {
                map.put(cur.h, map.getOrDefault(cur.h, 0) + 1);
            }
            else {
                int count = map.get(cur.h);
                if (count - 1 == 0) {
                    map.remove(cur.h);
                }
                else {
                    map.put(cur.h, --count);
                }
            }
            curHeight = map.lastKey();

            if (maxHeight != curHeight) {
                result.add(new int[]{cur.x, curHeight});
                maxHeight = curHeight;
            }
        }
        return result;

    }
}

