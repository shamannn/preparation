import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * <p>
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * <p>
 * Example:
 * <p>
 * Given nums = [2, 7, 11, 15], target = 9,
 * <p>
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 */
public class TwoSum_1 {
    public int[] twoSum(int[] nums, int target) {
        if (null == nums || nums.length < 1) {
            return null;
        }

        int[] result = new int[2];

        Map<Integer, Integer> matchMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer match = matchMap.get(target - nums[i]);
            if (match != null) {
                result[0] = match;
                result[1] = i;

                return result;
            }
            else {
                matchMap.put(nums[i], i);
            }
        }

        return result;
    }
}
