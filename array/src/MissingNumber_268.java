/**
 * Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.
 * <p>
 * Example 1:
 * <p>
 * Input: [3,0,1]
 * Output: 2
 * Example 2:
 * <p>
 * Input: [9,6,4,2,3,5,7,0,1]
 * Output: 8
 * Note:
 * Your algorithm should run in linear runtime complexity. Could you implement it using only constant extra space complexity?
 */
public class MissingNumber_268 {
    public static void main(String[] args) {
        System.out.println("missingNumber(new int[]{2, 3, 4, 5, 6, 7, 0}) = "
                + missingNumber(new int[]{2, 3, 4, 5, 6, 7, 0}));
    }

    public static int missingNumber(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }

        long sum = 0; // long to avoid overflow
        long shouldBeSum = nums.length; // key point!

        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            shouldBeSum += i;
        }

        return (int) (shouldBeSum - sum);
    }
}