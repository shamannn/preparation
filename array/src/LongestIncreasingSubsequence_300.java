import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Given an unsorted array of integers, find the length of longest increasing subsequence.
 * <p>
 * Example:
 * <p>
 * Input: [10,9,2,5,3,7,101,18]
 * Output: 4
 * Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.
 * Note:
 * <p>
 * There may be more than one LIS combination, it is only necessary for you to return the length.
 * Your algorithm should run in O(n2) complexity.
 * Follow up: Could you improve it to O(n log n) time complexity?
 */
public class LongestIncreasingSubsequence_300 {
    public int lengthOfLIS(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        else if (nums.length == 1) {
            return 1;
        }

        // so we incrementally build the longest increasing subsequence seq and then return its length
        List<Integer> seq = new ArrayList<>();
        for (int num : nums) {
            int ins = Collections.binarySearch(seq, num);
            if (ins < 0) {
                ins = -(ins + 1);
                if (ins == seq.size()) {
                    seq.add(num);// append new biggest
                }
                else {
                    seq.set(ins, num);             // replace bigger one
                }
            } /* else ignore exist num */
        }
        return seq.size();
    }
}

