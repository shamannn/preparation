/**
 * Given an integer array nums,
 * find the contiguous subarray within an array (containing at least one number) which has the largest product.
 * <p>
 * ****************************Solution**********************
 * We go from left to right to find max product. But we can hit negative number at any point so we want subarray
 * without this last number. Essentially this is same as coming from right to left.
 */
public class MaxProductOfSubarrays_152 {

    public static void main(String[] args) {
        int max = maxProduct(new int[]{0, -1, 4, -4, 5, -2, -1, -1, -2, -3, 0, -3, 0, 1, -1, -4, 4, 6, 2, 3,
                0, -5, 2, 1, -4, -2, -1, 3, -4, -6, 0, 2, 2, -1, -5, 1, 1, 5, -6, 2, 1, -3, -6, -6, -3, 4, 0, -2, 0, 2});
        System.out.println("max = " + max);
    }


    public static int maxProduct(int[] nums) {

        if (nums == null || nums.length == 0) {
            return Integer.MIN_VALUE; // ?
        }
        else if (nums.length == 1) {
            return nums[0];
        }
        else {
            int localMax = Integer.MIN_VALUE;
            int product = 0;

            // from bottom to top
            for (int i = 0; i < nums.length; i++) {
                if (product == 0) {
                    product = nums[i];
                }
                else {
                    product *= nums[i];
                }

                //System.out.println("1 i  " + i);
                //System.out.println("1 product " + product);

                localMax = Math.max(localMax, product);
            }

            //System.out.println("1 localMax " + localMax);

            // from top to bottom
            product = 0;

            for (int i = nums.length - 1; i >= 0; i--) {
                if (product == 0) {
                    product = nums[i];
                }
                else {
                    product *= nums[i];
                }

                //System.out.println("2 i  " + i);
                //  System.out.println("2 product " + product);

                localMax = Math.max(localMax, product);
            }
            //System.out.println("2 localMax " + localMax);
            return localMax;
        }
    }
}
