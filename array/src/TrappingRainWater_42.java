/**
 * Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.
 * <p>
 * <p>
 * The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped. Thanks Marcos for contributing this image!
 * <p>
 * Example:
 * <p>
 * Input: [0,1,0,2,1,0,1,3,2,1,2,1]
 * Output: 6
 */
public class TrappingRainWater_42 {
    public int trap(int[] A) {
        int leftIndex = 0;
        int rightIndex = A.length - 1;
        int max = 0;
        int leftmax = 0;
        int rightmax = 0;
        while (leftIndex <= rightIndex) {
            leftmax = Math.max(leftmax, A[leftIndex]);
            rightmax = Math.max(rightmax, A[rightIndex]);
            if (leftmax < rightmax) {
                max += (leftmax - A[leftIndex]);       // leftmax is smaller than rightmax, so the (leftmax-A[leftIndex]) water can be stored
                leftIndex++;
            }
            else {
                max += (rightmax - A[rightIndex]);
                rightIndex--;
            }
        }
        return max;
    }
}
