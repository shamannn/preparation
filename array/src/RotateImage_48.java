/**
 * You are given an n x n 2D matrix representing an image.
 * <p>
 * Rotate the image by 90 degrees (clockwise).
 * <p>
 * Note:
 * <p>
 * You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.
 * <p>
 * Example 1:
 * <p>
 * Given input matrix =
 * [
 * [1,2,3],
 * [4,5,6],
 * [7,8,9]
 * ],
 * <p>
 * rotate the input matrix in-place such that it becomes:
 * [
 * [7,4,1],
 * [8,5,2],
 * [9,6,3]
 * ]
 * Example 2:
 * <p>
 * Given input matrix =
 * [
 * [ 5, 1, 9,11],
 * [ 2, 4, 8,10],
 * [13, 3, 6, 7],
 * [15,14,12,16]
 * ],
 * <p>
 * rotate the input matrix in-place such that it becomes:
 * [
 * [15,13, 2, 5],
 * [14, 3, 4, 1],
 * [12, 6, 8, 9],
 * [16, 7,10,11]
 * ]
 */
public class RotateImage_48 {
    public void rotate(int[][] matrix) {
        if (matrix == null) {
            return;
        }

        for (int layer = 0; layer < matrix.length / 2; layer++) {
            for (int j = layer; j < matrix.length - layer - 1; j++) {
                int temp = matrix[layer][j]; // saving top to temp

                /*
                System.out.print("layer" + layer + " ");
                System.out.print("j" + j + " ");
                System.out.print("temp" + temp + " ");
                System.out.print("left" + matrix[matrix.length - j - 1][layer] + " ");
                System.out.print("bottom" + matrix[matrix.length - layer - 1][matrix.length - j - 1] + " ");
                System.out.println("right" + matrix[j][matrix.length - layer - 1] + " ");
                */


                matrix[layer][j] = matrix[matrix.length - j - 1][layer]; // left to top
                matrix[matrix.length - j - 1][layer] = matrix[matrix.length - layer - 1][matrix.length - j - 1]; // bottom to left
                matrix[matrix.length - layer - 1][matrix.length - j - 1] = matrix[j][matrix.length - layer - 1]; // right to bottom
                matrix[j][matrix.length - layer - 1] = temp; // temp to right
            }
        }

    }
}

