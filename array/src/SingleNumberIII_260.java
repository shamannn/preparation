/**
 * Given an array of numbers nums, in which exactly two elements appear only once and all the other elements appear exactly twice. Find the two elements that appear only once.
 * <p>
 * Example:
 * <p>
 * Input:  [1,2,1,3,2,5]
 * Output: [3,5]
 * Note:
 * <p>
 * The order of the result is not important. So in the above example, [5, 3] is also correct.
 * Your algorithm should run in linear runtime complexity. Could you implement it using only constant space complexity?
 */
public class SingleNumberIII_260 {
    /**
     * Once again, we need to use XOR to solve this problem. But this time, we need to do it in two passes:
     * <p>
     * In the first pass, we XOR all elements in the array, and get the XOR of the two numbers we need to find. Note that since the two numbers are distinct, so there must be a set bit (that is, the bit with value '1') in the XOR result. Find
     * out an arbitrary set bit (for example, the rightmost set bit).
     * <p>
     * In the second pass, we divide all numbers into two groups, one with the aforementioned bit set, another with the aforementinoed bit unset. Two different numbers we need to find must fall into thte two distrinct groups. XOR numbers in each group, we can find a number in either group.
     * <p>
     * Complexity:
     * <p>
     * Time: O (n)
     * <p>
     * Space: O (1)
     */
    public int[] singleNumber(int[] nums) {
        if (nums == null) {
            return null;
        }

        int xor = 0;

        for (int i : nums) {
            xor ^= i;
        }

        System.out.println(xor);

        xor &= -xor; // Get its last set bit

        System.out.println(xor);

        int[] result = new int[]{0, 0};

        for (int i : nums) {
            // TODO 2 groups
            if ((i & xor) == 0) {
                result[0] ^= i;
            }
            else {
                result[1] ^= i;
            }
        }

        return result;
    }
}
