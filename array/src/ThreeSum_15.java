import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
 * <p>
 * Note:
 * <p>
 * The solution set must not contain duplicate triplets.
 * <p>
 * Example:
 * <p>
 * Given array nums = [-1, 0, 1, 2, -1, -4],
 * <p>
 * A solution set is:
 * [
 * [-1, 0, 1],
 * [-1, -1, 2]
 * ]
 */
public class ThreeSum_15 {

    /**
     * This solution is O(n^2) time and O(1) space.
     * <p>
     * No additional data structures used - we sort numbers and then inside the loop we use two pointers (left and right)
     * to iterate through remaining numbers from both sides.
     * We also skip duplicates - as array is sorted they will be next to each other.
     */
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums == null || nums.length < 3) {
            return new LinkedList<>();
        }

        List<List<Integer>> result = new ArrayList<>();

        Arrays.sort(nums); // sort array
        for (int i = 0; i < nums.length - 2; i++) {

            if (i > 0 && nums[i] == nums[i - 1]) { // avoiding duplicates
                continue;
            }

            int left = i + 1, right = nums.length - 1;

            while (left < right) { // two pointers
                int sum = nums[i] + nums[left] + nums[right];
                if (sum == 0) {
                    result.add(Arrays.asList(nums[i], nums[left], nums[right]));

                    while (left + 1 < right && nums[left] == nums[left + 1]) { // avoiding duplicates
                        left++;
                    }
                    while (right - 1 > left && nums[right] == nums[right - 1]) { // avoiding duplicates
                        right--;
                    }
                    left++;
                    right--;
                }
                else if (sum < 0) {
                    left++;
                }
                else {
                    right--;
                }
            }
        }


        return result;

    }
}

