/**
 * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
 * <p>
 * Note:
 * <p>
 * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 * <p>
 * Example 1:
 * <p>
 * Input: [2,2,1]
 * Output: 1
 * Example 2:
 * <p>
 * Input: [4,1,2,1,2]
 * Output: 4
 */
public class SingleNumber_136 {
    public static void main(String[] args) {
        System.out.println("singleNumber(new int[]{1,2,3,4,4,3,2}) = " + singleNumber(new int[]{1, 2, 3, 4, 4, 3, 2}));
    }

    /**
     * we use bitwise XOR to solve this problem :
     * <p>
     * first , we have to know the bitwise XOR in java
     * <p>
     * 0 ^ N = N
     * N ^ N = 0
     * So..... if N is the single number
     * <p>
     * N1 ^ N1 ^ N2 ^ N2 ^..............^ Nx ^ Nx ^ N
     * <p>
     * = (N1^N1) ^ (N2^N2) ^..............^ (Nx^Nx) ^ N
     * <p>
     * = 0 ^ 0 ^ ..........^ 0 ^ N
     * <p>
     * = N
     */
    public static int singleNumber(int[] nums) {
        if (nums == null) {
            return -1;
        }

        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            result ^= nums[i];
        }

        return result;
    }
}