import java.util.HashSet;
import java.util.Set;

/**
 * Given a non-empty array of numbers, a0, a1, a2, … , an-1, where 0 ≤ ai < 231.
 * <p>
 * Find the maximum result of ai XOR aj, where 0 ≤ i, j < n.
 * <p>
 * Could you do this in O(n) runtime?
 * <p>
 * Example:
 * <p>
 * Input: [3, 10, 5, 25, 2, 8]
 * <p>
 * Output: 28
 * <p>
 * Explanation: The maximum result is 5 ^ 25 = 28.
 */
public class MaximumXORofTwoNumbersInArray_421 {
    public static void main(String[] args) {
        System.out.println("max(new int[]{[3, 10, 5, 25, 2, 8}) = " + max(new int[]{3, 10, 5, 25, 2, 8}));
    }

    /**
     * This solution is close to magic.
     * Variation of 2sum: see below for the analogy
     * The classic problem of 2sum actually achieved the acceleration from O(N^2) to O(N) in a way very similar to the second loop in this algorithm.
     * <p>
     * In 2sum, we want to find two numbers so that a + b = sum, naive way is to generate all pairs, and test each pair. This takes time O(N^2).
     * Instead, we only look at one number at each iteration, and try to find sum - a in the HashMap.
     * In this problem (the second part where you try to update max): we want to find two prefixs out of the set so that prefix1 xor prefix2 = tmp, with tmp being the greedily updated max.
     * Instead, we only look at one prefix at each iteration, and try to find tmp ^ prefix1 instead.
     * of course, this part is a little more complicated than the subtraction in 2sum: we have to know the thing about a xor b = c ⇒ a xor c = b, and b xor c = a.
     * Note that both approaches benefitted from the O(1) lookup provided by hashing data structures.
     */
    public static int max(int[] nums) {
        if (nums == null) {
            return 0;
        }
        /*The max is a record of the largest XOR we got so far. if it's 11100 at i = 2, it means
        before we reach the last two bits, 11100 is the biggest XOR we have, and we're going to explore
        whether we can get another two '1's and put them into maxResult

        This is a greedy part, since we're looking for the largest XOR, we start
        from the very begining, aka, the 31st postition of bits. */
        int max = 0, mask = 0;
        for (int i = 30; i >= 0; i--) {
            //The mask will grow like  100..000 , 110..000, 111..000,  then 1111...111
            //for each iteration, we only care about the left parts
            mask |= (1 << i);

            // the set of "i" most significant bits of nums
            Set<Integer> set = new HashSet<>();
            for (int num : nums) {
                int leftPartOfNum = num & mask;
                set.add(leftPartOfNum);
            }

            // if i = 1 and before this iteration, the maxResult we have now is 1100,
            // my wish is the maxResult will grow to 1110, so I will try to find a candidate
            // which can give me the greedyTry;
            int greedyTry = max | (1 << i);

            for (int leftPartOfNum : set) { // Greedy search based on prefix
                //This is the most tricky part, coming from a fact that if a ^ b = c, then a ^ c = b;
                // now we have the 'c', which is greedyTry, and we have the 'a', which is leftPartOfNum
                // If we hope the formula a ^ b = c to be valid, then we need the b,
                // and to get b, we need a ^ c, if a ^ c existed in our set, then we're good to go
                if (set.contains(leftPartOfNum ^ greedyTry)) {
                    max = greedyTry;
                    break;
                }
            }

            // If unfortunately, we didn't get the greedyTry, we still have our max,
            // So after this iteration, the max will stay at 1100.
        }

        return max;
    }

}
